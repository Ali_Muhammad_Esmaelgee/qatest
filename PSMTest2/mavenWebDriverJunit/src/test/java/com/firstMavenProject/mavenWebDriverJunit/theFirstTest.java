package com.firstMavenProject.mavenWebDriverJunit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

public class theFirstTest {

	
	HtmlUnitDriver a;
	
	@Before
	public void setUp()
	{
		 a = new HtmlUnitDriver();
		 a.get("http://www.google.com");
	}
	
	@After
	public void tearDown()
	{
		a.close();
	}
	
	
	@Test
	public void checkTitle()
	{
		System.out.println("The title is now: "+a.getTitle());
	}
	
	
}
