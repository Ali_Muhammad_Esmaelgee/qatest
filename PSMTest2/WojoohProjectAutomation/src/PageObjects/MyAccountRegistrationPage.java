/**
 * 
 */
package PageObjects;

import java.util.Calendar;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import AutomatedScripts.WojoohFunctions;
import wojoohLocators.HomepageLocators;
import wojoohLocators.MyAccountLoginLocator;
import wojoohLocators.MyAccountRegistrationLocators;
import org.openqa.selenium.support.ui.Select;

/**
 * @author Yassar
 * @category Performs operations on Registration pages only
 * @version 1.0
 * @fillFormGeneral  - Populates all the fields required for General registration type
 * @checkLoyaltyCheckboxStatus
 * @fillFormLoyalty
 * @fillFormLoyalty
 * @fillDate
 * @goToPage
 * @currentEmailsAcc
 * @checkExistingEmails
 * @checkPasswordValidation
 * @checkConfirmationEmail
 * @checkConfirmationEmail
 * @checkDOBvalidation
 * @checkMandatoryFieldsForLoyalty
 * @checkMandatoryFields
 * @checkElementsOnPage
 *  
 */
public class MyAccountRegistrationPage extends MyAccountRegistrationLocators{

	//implement following	
	//2. create assertionMethod
	
	public MyAccountRegistrationPage() {
		// TODO Auto-generated constructor stub
		currentEmailsAcc(Integer.parseInt(mailSelector));
	}
		
	public void fillFormGeneral(){
		
		
		System.out.println("fill form general");
		checkCurrentPage("customer/account/create/");
		checkDropDown();
		
		String firstname = "Jacks";
		String lastname = "Stones";
		By thisLanguageSelector = radioLangSelectorEng;
		
		if(storeview.indexOf("ar") >= 0){
			firstname = "جاك";
			lastname = "حجر";
			thisLanguageSelector = radioLangSelectorAr;
		}
		
		
		checkDropDown();
		a.findElement(fieldFirstname).clear();
		a.findElement(fieldFirstname).sendKeys(firstname);
		checkDropDown();
		a.findElement(fieldLastname).clear();
		a.findElement(fieldLastname).sendKeys(lastname);
		checkDropDown();
		a.findElement(fieldEmail).clear();
		a.findElement(fieldEmail).sendKeys(email);
		checkDropDown();
		a.findElement(thisLanguageSelector).click();
		checkDropDown();
		a.findElement(fieldPassword).clear();		
		a.findElement(fieldPassword).sendKeys("password123");
		checkDropDown();
		a.findElement(fieldConfPassword).clear();
		a.findElement(fieldConfPassword).sendKeys("password123");
		checkDropDown();
	}
		
	public void fillFormGeneral(String theEmail)
	{
	
		checkCurrentPage("customer/account/create/");
		
		String firstname = "Jacks";
		String lastname = "Stones";
		By thisLanguageSelector = radioLangSelectorEng;
		
		if(storeview.indexOf("ar") >= 0){
			firstname = "جاك";
			lastname = "حجر";
			thisLanguageSelector = radioLangSelectorAr;
		}
		
		checkLoyaltyCheckboxStatus(0);
		
		a.findElement(fieldFirstname).clear();
		a.findElement(fieldFirstname).sendKeys(firstname);
		a.findElement(fieldLastname).clear();
		a.findElement(fieldLastname).sendKeys(lastname);
		a.findElement(fieldEmail).clear();
		a.findElement(fieldEmail).sendKeys(theEmail);
		a.findElement(thisLanguageSelector).click();
		a.findElement(fieldPassword).clear();
		a.findElement(fieldPassword).sendKeys("password123");
		a.findElement(fieldConfPassword).clear();
		a.findElement(fieldConfPassword).sendKeys("password123");
	}
    	
	public void checkLoyaltyCheckboxStatus(int selector)
	{
		if(selector == 0){//assert that Rewards is disabled
			if(Integer.parseInt(a.findElement(checkboxLoyaltyEnrolment).getAttribute("value")) == 1){
				a.findElement(checkboxLoyaltyEnrolment).click();
			}
		}else { // assert that Rewards is enabled
			if(Integer.parseInt(a.findElement(checkboxLoyaltyEnrolment).getAttribute("value")) == 0){
				a.findElement(checkboxLoyaltyEnrolment).click();
			}
		}
	}
	
	public void fillFormLoyalty(){
		
		
		String currTestArea = "FillFormLoyalty";
		System.out.println(currTestArea);
		
		checkCurrentPage("customer/account/create/");
		
		String firstname = "Jacks";
		String lastname = "Stones";
		By thisLanguageSelector = radioLangSelectorEng;
		
		if(storeview.indexOf("ar") >= 0){
			firstname = "جاك";
			lastname = "حجر";
			thisLanguageSelector = radioLangSelectorAr;
		}
		
		a.findElement(fieldFirstname).clear();
			a.findElement(fieldFirstname).sendKeys(firstname);				
		a.findElement(fieldLastname).clear();
			a.findElement(fieldLastname).sendKeys(lastname);		
		a.findElement(fieldEmail).clear();		
			a.findElement(fieldEmail).sendKeys(email);
			a.findElement(thisLanguageSelector).click();
		checkLoyaltyCheckboxStatus(1);				
		a.findElement(fieldDay).clear(); 	
			a.findElement(fieldDay).sendKeys("10");		
		a.findElement(fieldMonth).clear(); 	
			a.findElement(fieldMonth).sendKeys("07");					
		a.findElement(fieldYear).clear(); 	
			a.findElement(fieldYear).sendKeys("1988");
		a.findElement(radioMale).click();
		a.findElement(fieldCellphone).clear();
		a.findElement(fieldCellphone).sendKeys(createCellNo());
		a.findElement(checkboxTermsandconditions).click();		
		a.findElement(fieldPassword).clear();
			a.findElement(fieldPassword).sendKeys("password123");		
		a.findElement(fieldConfPassword).clear();
			a.findElement(fieldConfPassword).sendKeys("password123");		
	}
		
	public void fillFormLoyalty(String theEmail){
		
	checkCurrentPage("customer/account/create/");
	
		String firstname = "Jacks";
		String lastname = "Stones";
		By thisLanguageSelector = radioLangSelectorEng;
		
		if(storeview.indexOf("ar") >= 0){
			firstname = "جاك";
			lastname = "حجر";
			thisLanguageSelector = radioLangSelectorAr;
		}
		
		a.findElement(fieldFirstname).clear();
			a.findElement(fieldFirstname).sendKeys(firstname);				
		a.findElement(fieldLastname).clear();
			a.findElement(fieldLastname).sendKeys(lastname);		
		a.findElement(fieldEmail).clear();		
			a.findElement(fieldEmail).sendKeys(theEmail);
			a.findElement(thisLanguageSelector).click();
		checkLoyaltyCheckboxStatus(1);				
		a.findElement(fieldDay).clear(); 	
			a.findElement(fieldDay).sendKeys("10");		
		a.findElement(fieldMonth).clear(); 	
			a.findElement(fieldMonth).sendKeys("07");					
		a.findElement(fieldYear).clear(); 	
			a.findElement(fieldYear).sendKeys("1988");
		a.findElement(radioMale).click();
		a.findElement(fieldCellphone).clear();
		a.findElement(fieldCellphone).sendKeys(0547322133+"");
		a.findElement(checkboxTermsandconditions).click();		
		a.findElement(fieldPassword).clear();
			a.findElement(fieldPassword).sendKeys("password123");		
		a.findElement(fieldConfPassword).clear();
			a.findElement(fieldConfPassword).sendKeys("password123");		
	}


 	public void fillDate(String d, String m, String y)
	{		
		a.findElement(fieldDay).clear();
		a.findElement(fieldDay).sendKeys(d);
		a.findElement(fieldMonth).clear();
		a.findElement(fieldMonth).sendKeys(m);
		a.findElement(fieldYear).clear();
		a.findElement(fieldYear).sendKeys(y);
		a.findElement(buttonSubmit).click();
	}
	
	public void goToPage()
	{		
		
			a.findElement(HomepageLocators.dropMyAccount).click();			
			a.findElement(HomepageLocators.linkRegister).click();			
			checkCurrentPage("customer/account/create/");
	}
	
	
	/**
	 * Description of the function
	 * 
	 * @param Type Integer
	 * @param value 1001 return mail for Stag | Arabic | KSA | Rewards
	 * @param value 1101 return mail for Stag | English | KSA | Rewards
	 * @param value 1000 return mail for Stag | Arabic | KSA | General  
	 * @param value 1100 return mail for Stag | English | KSA | General 
	 * @param value 1011 return mail for Stag | Arabic | UAE | Rewards
	 * @param value 1111 return mail for Stag | English | UAE | Rewards
	 * @param value 1110 return mail for Stag | English | UAE | General
	 * @param valye 1010 return mail for Stag | Arabic | UAE | General
	 */
	public void currentEmailsAcc(int i)
	{
		String email = "";
		switch(i)	{		
		case 1001:		email = "RBDTest_1447936246708@mailinator.com";		break;		
		case 1101:		email = "RBDTest_1447936172876@mailinator.com";		break;		
		case 1000:		email = "RBDTest_1447869552494@mailinator.com";		break;		
		case 1100:		email = "RBDTest_1447933862827@mailinator.com";		break;		
		case 1011:		email = "RBDTest_1447936409150@mailinator.com";		break;		
		case 1111:		email = "RBDTest_1447952078691@mailinator.com";		break;		
		case 1110:		email = "RBDTest_1447936079048@mailinator.com";		break;		
		case 1010:		email = "RBDTest_1448005617163@mailinator.com";		break;
		}		
				
		testEmail = email;
	}
	
	
	//check for existing emails test
	public void checkExistingEmails(String account)
	{
		checkCurrentPage("customer/account/create/");
		String currTestArea = "Checking Existing Mails for "+account+" Accounts";
		System.out.println(currTestArea);
		
		
		
		if(Integer.parseInt(a.findElement(checkboxLoyaltyEnrolment).getAttribute("value")) == 1){
			a.findElement(checkboxLoyaltyEnrolment).click();
		}
		
		String theEmail = testEmail;
		String errMessage = "";
		if(account.equals("loyalty") && store.equals("sa")){			
			errMessage = "YOU ARE ALREADY SUBSCRIBED TO REWARDS PROGRAMME. PLEASE USE YOUR CREDENTIALS TO LOGIN.";			
		}else if(account.equals("general") && store.equals("sa")){			
			errMessage = "THERE IS ALREADY AN ACCOUNT WITH THIS EMAIL ADDRESS. IF YOU ARE SURE THAT IT IS YOUR EMAIL ADDRESS, CLICK HERE TO GET YOUR PASSWORD AND ACCESS YOUR ACCOUNT.";
		}else if(account.equals("loyalty") && store.equals("ae")){			
			errMessage = "YOU ARE ALREADY SUBSCRIBED TO REWARDS PROGRAMME. PLEASE USE YOUR CREDENTIALS TO LOGIN.";			
		}else if(account.equals("general") && store.equals("ae")){			
			errMessage = "THERE IS ALREADY AN ACCOUNT WITH THIS EMAIL ADDRESS. IF YOU ARE SURE THAT IT IS YOUR EMAIL ADDRESS, CLICK HERE TO GET YOUR PASSWORD AND ACCESS YOUR ACCOUNT.";
		}
		
		
		fillFormGeneral(theEmail);			
		a.findElement(buttonSubmit).click();
		
		
		try {		
			Assert.assertEquals(a.findElement(MyAccountLoginPage.headerMessage).getText(),errMessage);			
		}catch(Throwable err){
			c(err+"","Testing Existing Email for "+account+" Account type with test Email - "+theEmail,0);
		}
		
		checkCurrentPage("customer/account/create/");		
	}
	
	//check length of password and password matching
	public void checkPasswordValidation()
	{
		String currTestArea = "Checking Password Validation";
		System.out.println(currTestArea);
		checkCurrentPage("customer/account/create/");
		
		String passValidityMessage = "Please enter 6 or more characters. Leading or trailing spaces will be ignored.";
		String confPassMessage = "Please make sure your passwords match.";
		
		if(storeview.indexOf("ar") >= 0){
			passValidityMessage = "يرجى إدخال 6 رموز أو أكثر. . سوف يتم تجاهل مسافات التوجيه أو المسافات الزائدة";
			confPassMessage = "يرجى التأكد من تطابق كلمات المرور خاصتك.";
		}
		
		
		fillFormGeneral(testEmail);
		a.findElement(fieldPassword).clear();
		a.findElement(fieldPassword).sendKeys("asd");
		a.findElement(fieldConfPassword).clear();		
		a.findElement(fieldConfPassword).sendKeys("asd");
		a.findElement(buttonSubmit).click();
		
		waiterText(textPasswordError, passValidityMessage);
		
		try{
			Assert.assertEquals(a.findElement(textPasswordError).getText(), passValidityMessage);
		}catch(AssertionError err){
			c(err+"",0);
		}
		
		
		a.findElement(fieldPassword).sendKeys("password123");
		a.findElement(fieldConfPassword).sendKeys("asd");
		a.findElement(buttonSubmit).click();
		
		
		try{
			Assert.assertEquals(a.findElement(textConfPassError).getText(), confPassMessage);			
		}catch(AssertionError err){
			c(err+"",0);
		}
		
		checkCurrentPage("customer/account/create/");
	}
	
	//check confirmation email on mailinator with the email in the current running test
	public void checkConfirmationEmail()
	{
		email = "RBDTest_1447072566350@mailinator.com"; //codat
		String emailName = email; 
		emailName = email.substring(0,emailName.indexOf("@")); 
		
		a.get("https://mailinator.com/inbox.jsp?to="+emailName);
		
		waiterText(mailinatorEmail, "Account Creation Email",200);
		
		a.findElement(mailinatorEmail).click();
		
		a.switchTo().frame(1);
		
		waiterText(linkAccValidation, "Please click here to validate your account");
		
		a.findElement(linkAccValidation).click();		
	}
	
	//check confirmation email on mailinator with the email in paramters
	public void checkConfirmationEmail(String theEmail)
	{
		String emailName; 
		emailName = theEmail.substring(0,theEmail.indexOf("@")); 
		
		a.get("https://mailinator.com/inbox.jsp?to="+emailName);
		
		waiterText(mailinatorEmail, "Account Creation Email",200);
		
		a.findElement(mailinatorEmail).click();
		
		a.switchTo().frame(1);
		
		waiterText(linkAccValidation, "Please click here to validate your account");
		
		String theCrmLink = a.findElement(linkAccValidation).getAttribute("href");
		
		a.findElement(linkAccValidation).click();
		
		
				
		System.out.println(theCrmLink);
		
		theCrmLink = "http://stag:Dubai@"+theCrmLink.substring(7);

		System.out.println(theCrmLink);
		
		a.get(MyAccountLoginPage.theUrl);
	}
	
	public void checkDOBvalidation()
	{

		String currTestArea = "Checking DOB Validation Loyalty registration";
		System.out.println(currTestArea);
		checkCurrentPage("customer/account/create/");
		
		checkLoyaltyCheckboxStatus(1);		
		
		fillFormLoyalty(testEmail);
		
		String frebDateErrMsg = "Please enter a valid day (1-28).";		
		
		if(storeview.equals("ar")){
			frebDateErrMsg = "الرجاء الاختيار بين (1-28)";			
		}
		
		//wrong input
			// alphabets instead of numbers				
				// test for days
				fillDate("as", "01", "1995");				
				assertThis(textReqDateValidFullDate, "Please enter a valid full date.", "Assertion of Error Message on inserting 'as' in Day Field");
								
				// test for month				
				fillDate("01","as","1995");				
				assertThis(textReqDateValidFullDate, "Please enter a valid month (1-12).", "Assertion of Error Message on inserting 'as' in Month Field");				
				
				// test for year				
				fillDate("01","01","asdf");				
				assertThis(textReqDateValidFullDate, "Please enter a valid full date.", "Assertion of Error Message on inserting 'asdf' in Year Field");										
				
			// negative numbers
				//testing days field
				fillDate("-1","01","1995");				
				assertThis(textReqDateValidFullDate, "Please enter a valid full date.", "Assertion of Error Message on inserting '-1' in Day Field");
								
				// test for month field
				fillDate("01","-1","1995");				
				assertThis(textReqDateValidFullDate, "Please enter a valid month (1-12).", "Assertion of Error Message on inserting '-1' in Month Field");
				
				// test for year field
				fillDate("01","01","1995");				
				assertThis(textReqDateValidFullDate, "Please enter a valid full date.", "Assertion of Error Message on inserting '-1988' in Year Field");
				
		// leap years validation
			//loop for all leap years from 1970
				int currYear = Calendar.getInstance().get(Calendar.YEAR);
				
				for(int i=currYear-30; i <= currYear ; i++){
					if((i%4) != 0){
						fillDate("29","02",i+"");						
						a.findElement(buttonSubmit).click();											
						
						try{
							Assert.assertEquals(a.findElement(textReqDateValidFullDate).getText(), frebDateErrMsg);
						}catch(AssertionError err){
							c(err+"","Assertion of Error Message for inserting 29 Days for Month of February for Leap years");
						}
						
						}
					}
			

		//age less than 18years						
			//around 16years						
			fillFormLoyalty();
			fillDate("5", "5", currYear-16+"");
			
			a.findElement(buttonSubmit).click();
			
			String above18ErrMsg = "CUSTOMER MUST BE OVER 18 TO JOIN";
			
			if(storeview.equals("ar")){
				above18ErrMsg = "يجب أن يكون عمر العميل 18 سنة على الاقل للانضمام";
			}			
			assertThis(textHeaderError, above18ErrMsg , "Assertion of Error Message for inserting Age below 18 Years Old");			
			
			checkCurrentPage("customer/account/create/");	
	}
			
	public void checkMandatoryFieldsForLoyalty()
	{		
		String currTestArea = "Checking Mandatory Fields for Loyalty registration";
		System.out.println(currTestArea);
		
		checkCurrentPage("customer/account/create/");
		//Asserting All required fields without any input
		a.findElement(checkboxLoyaltyEnrolment).click();
		a.findElement(buttonSubmit).click();
		
		String reqFieldMessage = "Required field.";
		String radioReqMessage = "Please select one of the above options.";
		String dobReqMessage = "This date is a required value.";
		
		if(storeview.indexOf("ar") >= 0){
			reqFieldMessage = "هذا الحقل مطلوب.";
			radioReqMessage = "يرجى تحديد واحد من الخيارات أعلاه.";
			dobReqMessage = "هذا التاريخ هو القيمة المطلوبة.";
		}
		
				
		String prePage ="Asserting 'Required Fields' error messages for omitted fields - First attempt - All Mandatory fields Empty";		
		try{
			Assert.assertEquals(a.findElement(textReqFirstname).getText(), reqFieldMessage);
			Assert.assertEquals(a.findElement(textReqLastname).getText(), reqFieldMessage);
			Assert.assertEquals(a.findElement(textReqEmail).getText(), reqFieldMessage);
			Assert.assertEquals(a.findElement(textReqLangSelector).getText(), radioReqMessage);
			
			Assert.assertEquals(a.findElement(textReqDateValidFullDate).getText(), dobReqMessage);
			Assert.assertEquals(a.findElement(textReqGender).getText(), radioReqMessage);
			Assert.assertEquals(a.findElement(textReqCellphone).getText(), reqFieldMessage);			
			
			Assert.assertEquals(a.findElement(textReqPasswordError).getText(), reqFieldMessage);
			Assert.assertEquals(a.findElement(textReqConfPassError).getText(), reqFieldMessage);
			c("Success",prePage);
		}catch (AssertionError err) {
			c(err+"",prePage);
		}
		
		
		
		//Asserting error message for omitted mandatory fields, and assertion of absence of error message for First name
		prePage="Asserting 'Required Fields' error messages for omitted fields - Second attempt - All Mandatory fields Empty except Firstname Field.";
		a.findElement(fieldFirstname).clear();
		a.findElement(fieldFirstname).sendKeys("Jack");
		a.findElement(buttonSubmit).click();			
		waiterText(textReqFirstname, "");
		
		try{			
			Assert.assertEquals(a.findElement(textReqLastname).getText(), reqFieldMessage);
			Assert.assertEquals(a.findElement(textReqEmail).getText(), reqFieldMessage);
			Assert.assertEquals(a.findElement(textReqLangSelector).getText(), radioReqMessage);
			Assert.assertEquals(a.findElement(textReqDateValidFullDate).getText(), dobReqMessage);
			Assert.assertEquals(a.findElement(textReqGender).getText(), radioReqMessage);
			Assert.assertEquals(a.findElement(textReqCellphone).getText(), reqFieldMessage);
			Assert.assertEquals(a.findElement(textReqPasswordError).getText(), reqFieldMessage);
			Assert.assertEquals(a.findElement(textReqConfPassError).getText(), reqFieldMessage);
			c("Success",prePage);
		}catch (AssertionError err) {
			c(err+"",prePage);
		}
		
		
		
		//Asserting error message for omitted mandatory fields, and assertion of absence of error message for Firstname and lastname 
		prePage="Asserting 'Required Fields' error messages for omitted fields - Second attempt - Only Filling Firstname and Lastname";
		a.findElement(fieldLastname).clear();
		a.findElement(fieldLastname).sendKeys("Stone");
		a.findElement(buttonSubmit).click();			
		waiterText(textReqLastname, "");
		
		try{		
			Assert.assertEquals(a.findElement(textReqEmail).getText(), reqFieldMessage);
			Assert.assertEquals(a.findElement(textReqLangSelector).getText(), radioReqMessage);
			Assert.assertEquals(a.findElement(textReqDateValidFullDate).getText(), dobReqMessage);
			Assert.assertEquals(a.findElement(textReqGender).getText(), radioReqMessage);
			Assert.assertEquals(a.findElement(textReqCellphone).getText(), reqFieldMessage);
			Assert.assertEquals(a.findElement(textReqPasswordError).getText(), reqFieldMessage);
			Assert.assertEquals(a.findElement(textReqConfPassError).getText(), reqFieldMessage);
			c("Success",prePage);
		}catch (AssertionError err) {
			c(err+"",prePage);
		}
		
		
		
		//Asserting error message for omitted mandatory fields, and assertion of absence of error message for Firstname, lastname  and Email address
		prePage="Asserting 'Required Fields' error messages for omitted fields - Third Attempt - Filling Firstname, Lastname and Email address";
		a.findElement(fieldEmail).clear();
		a.findElement(fieldEmail).sendKeys(testEmail);
		a.findElement(buttonSubmit).click();			
		waiterText(textReqEmail, "");
				
		try{			
			Assert.assertEquals(a.findElement(textReqLangSelector).getText(), radioReqMessage);
			Assert.assertEquals(a.findElement(textReqDateValidFullDate).getText(), dobReqMessage);
			Assert.assertEquals(a.findElement(textReqGender).getText(), radioReqMessage);
			Assert.assertEquals(a.findElement(textReqCellphone).getText(), reqFieldMessage);
			Assert.assertEquals(a.findElement(textReqPasswordError).getText(), reqFieldMessage);
			Assert.assertEquals(a.findElement(textReqConfPassError).getText(), reqFieldMessage);
			c("Success",prePage);
		}catch (AssertionError err) {
			c(err+"",prePage);
		}
		
		
		
		//Asserting error message for omitted mandatory fields, and assertion of absence of error message for Firstname, lastname , Email, and language pref
		prePage="Asserting 'Required Fields' error messages for omitted fields - Fourth Attempt - Filling Firstname, Lastname, Email address and Language preferences";		
		a.findElement(radioLangSelectorEng).click();
		a.findElement(buttonSubmit).click();			
		waiterText(textReqLangSelector, "");
		
		try{			
			Assert.assertEquals(a.findElement(textReqDateValidFullDate).getText(), dobReqMessage);
			Assert.assertEquals(a.findElement(textReqGender).getText(), radioReqMessage);
			Assert.assertEquals(a.findElement(textReqCellphone).getText(), reqFieldMessage);
			Assert.assertEquals(a.findElement(textReqPasswordError).getText(), reqFieldMessage);
			Assert.assertEquals(a.findElement(textReqConfPassError).getText(), reqFieldMessage);
			c("Success",prePage);
		}catch (AssertionError err) {
			c(err+"",prePage);
		}
		
		
		
		//Asserting error message for omitted mandatory fields, and assertion of absence of error message for Firstname, lastname , Email, lanauage pref, DOB
		prePage="Asserting 'Required Fields' error messages for omitted fields - Fifth Attempt - Filling firstname, Lastname, Email Address, Lang Preferences and DOB";
		fillDate("01", "01", "2000");		
		a.findElement(buttonSubmit).click();			
		waiterText(textReqDateValidFullDate, "");
		
		try{						
			Assert.assertEquals(a.findElement(textReqGender).getText(), radioReqMessage);
			Assert.assertEquals(a.findElement(textReqCellphone).getText(), reqFieldMessage);
			Assert.assertEquals(a.findElement(textReqPasswordError).getText(), reqFieldMessage);
			Assert.assertEquals(a.findElement(textReqConfPassError).getText(), reqFieldMessage);
			c("Success",prePage);
		}catch (AssertionError err) {
			c(err+"",prePage);
		}
		
		
		
		//Asserting error message for omitted mandatory fields, and assertion of absence of error message for Firstname, lastname  and Email address, language pref, DOB and Gender
		prePage="Asserting 'Required Fields' error messages for omitted fields - Sixth Attempt - Filling Firstname, Lastname, Email, Lang Preferences, DOB and Gender";		
		a.findElement(radioMale).click();
		a.findElement(buttonSubmit).click();			
		waiterText(textReqGender, "");
		
		try{			
			Assert.assertEquals(a.findElement(textReqCellphone).getText(), reqFieldMessage);
			Assert.assertEquals(a.findElement(textReqPasswordError).getText(), reqFieldMessage);
			Assert.assertEquals(a.findElement(textReqConfPassError).getText(), reqFieldMessage);
			c("Success",prePage);
		}catch (AssertionError err) {
			c(err+"",prePage);
		}
		
		
		
		//Asserting error message for omitted mandatory fields, and assertion of absence of error message for Firstname, lastname  and Email address, language pref, DOB, Gender and Mobile
		prePage="Asserting 'Required Fields' error messages for omitted fields - Seventh Attempt - Filling Firstname, Lastname, Email, Lang Preferences, DOB, Gender and Mobile";		
		a.findElement(fieldCellphone).clear();
		a.findElement(fieldCellphone).sendKeys("0512345654");
		a.findElement(buttonSubmit).click();			
		waiterText(textReqCellphone, "");
		
		try{						
			Assert.assertEquals(a.findElement(textReqPasswordError).getText(), reqFieldMessage);
			Assert.assertEquals(a.findElement(textReqConfPassError).getText(), reqFieldMessage);
			c("Success",prePage);
		}catch (AssertionError err) {
			c(err+"",prePage);
		}
		
		
		
		//Asserting error message for omitted mandatory fields, and assertion of absence of error message for Firstname, lastname  and Email address, language pref, DOB, Gender, Mobile and Password
		prePage="Asserting 'Required Fields' error messages for omitted fields - Eighth Attempt - Filling Firstname, Lastname, Email, Lang Preferences, DOB, Gender, Mobile and Password";		
		a.findElement(fieldPassword).clear();
		a.findElement(fieldPassword).sendKeys("password123");
		a.findElement(buttonSubmit).click();			
		waiterText(textReqPasswordError, "");
		
		try{									
			Assert.assertEquals(a.findElement(textReqConfPassError).getText(), reqFieldMessage);
			c("Success",prePage);
		}catch (AssertionError err) {
			c(err+"",prePage);
		}

		
		
		//Asserting error message for omitted mandatory fields, and assertion of absence of error message for Firstname, lastname  and Email address, language pref, DOB, Gender, Mobile, Password and confPassword
		prePage="Asserting 'Required Fields' error messages for omitted fields - Ninth Attempt - Filling Firstname, Lastname, Email, Lang Preferences, DOB, Gender, Mobile, Password and ConfPassword";		
		a.findElement(fieldConfPassword).clear();
		a.findElement(fieldConfPassword).sendKeys("password123");			
		a.findElement(buttonSubmit).click();
		waiterText(textReqConfPassError, "");
		checkDropDown();
	}	

	public void checkMandatoryFields()
	{		
		String currTestArea = "Checking Mandatory Fields for Loyalty registration";
		System.out.println(currTestArea);
		
		checkCurrentPage("customer/account/create/");
		//Asserting All required fields without any input		
		a.findElement(buttonSubmit).click();
		
		String reqFieldMessage = "Required field.";
		String radioReqMessage = "Please select one of the above options.";
		String dobReqMessage = "This date is a required value.";
		
		if(storeview.indexOf("ar") >= 0){
			reqFieldMessage = "هذا الحقل مطلوب.";
			radioReqMessage = "يرجى تحديد واحد من الخيارات أعلاه.";
			dobReqMessage = "هذا التاريخ هو القيمة المطلوبة.";
		}
		
				
		String prePage ="Asserting 'Required Fields' error messages for omitted fields - First attempt - All Mandatory fields Empty";		
		try{
			Assert.assertEquals(a.findElement(textReqFirstname).getText(), reqFieldMessage);
			Assert.assertEquals(a.findElement(textReqLastname).getText(), reqFieldMessage);
			Assert.assertEquals(a.findElement(textReqEmail).getText(), reqFieldMessage);
			Assert.assertEquals(a.findElement(textReqLangSelector).getText(), radioReqMessage);			
			Assert.assertEquals(a.findElement(textReqPasswordError).getText(), reqFieldMessage);
			Assert.assertEquals(a.findElement(textReqConfPassError).getText(), reqFieldMessage);
			c("Success",prePage);
		}catch (AssertionError err) {
			c(err+"",prePage);
		}
		
		
		
		//Asserting error message for omitted mandatory fields, and assertion of absence of error message for First name
		prePage ="Asserting 'Required Fields' error messages for omitted fields - Second attempt - All Mandatory fields Empty except Firstname Field.";
		a.findElement(fieldFirstname).clear();
		a.findElement(fieldFirstname).sendKeys("Jack");
		a.findElement(buttonSubmit).click();			
		waiterText(textReqFirstname, "");
		
		try{			
			Assert.assertEquals(a.findElement(textReqLastname).getText(), reqFieldMessage);
			Assert.assertEquals(a.findElement(textReqEmail).getText(), reqFieldMessage);
			Assert.assertEquals(a.findElement(textReqLangSelector).getText(), radioReqMessage);			
			Assert.assertEquals(a.findElement(textReqPasswordError).getText(), reqFieldMessage);
			Assert.assertEquals(a.findElement(textReqConfPassError).getText(), reqFieldMessage);
			c("Success",prePage);
		}catch (AssertionError err) {
			c(err+"",prePage);
		}
		
		
		//Asserting error message for omitted mandatory fields, and assertion of absence of error message for Firstname and lastname 
		prePage="Asserting 'Required Fields' error messages for omitted fields - Second attempt - Only Filling Firstname and Lastname";
		a.findElement(fieldLastname).clear();
		a.findElement(fieldLastname).sendKeys("Stone");
		a.findElement(buttonSubmit).click();			
		waiterText(textReqLastname, "");
		
		try{		
			Assert.assertEquals(a.findElement(textReqEmail).getText(), reqFieldMessage);
			Assert.assertEquals(a.findElement(textReqLangSelector).getText(), radioReqMessage);			
			Assert.assertEquals(a.findElement(textReqPasswordError).getText(), reqFieldMessage);
			Assert.assertEquals(a.findElement(textReqConfPassError).getText(), reqFieldMessage);
			c("Success",prePage);
		}catch (AssertionError err) {
			c(err+"",prePage);
		}
		
		
		//Asserting error message for omitted mandatory fields, and assertion of absence of error message for Firstname, lastname  and Email address
		prePage="Asserting 'Required Fields' error messages for omitted fields - Third Attempt - Filling Firstname, Lastname and Email address";
		a.findElement(fieldEmail).clear();
		a.findElement(fieldEmail).sendKeys(testEmail);
		a.findElement(buttonSubmit).click();			
		waiterText(textReqEmail, "");
				
		try{			
			Assert.assertEquals(a.findElement(textReqLangSelector).getText(), radioReqMessage);
			Assert.assertEquals(a.findElement(textReqPasswordError).getText(), reqFieldMessage);
			Assert.assertEquals(a.findElement(textReqConfPassError).getText(), reqFieldMessage);
			c("Success",prePage);
		}catch (AssertionError err) {
			c(err+"",prePage);
		}
		
		
		
		//Asserting error message for omitted mandatory fields, and assertion of absence of error message for Firstname, lastname , Email, and language pref
		prePage="Asserting 'Required Fields' error messages for omitted fields - Fourth Attempt - Filling Firstname, Lastname, Email address and Language preferences";		
		a.findElement(radioLangSelectorEng).click();
		a.findElement(buttonSubmit).click();			
		waiterText(textReqLangSelector, "");
		
		try{						
			Assert.assertEquals(a.findElement(textReqPasswordError).getText(), reqFieldMessage);
			Assert.assertEquals(a.findElement(textReqConfPassError).getText(), reqFieldMessage);
			c("Success",prePage);
		}catch (AssertionError err) {
			c(err+"",prePage);
		}
		
		
		//Asserting error message for omitted mandatory fields, and assertion of absence of error message for Firstname, lastname  and Email address, language pref, DOB, Gender, Mobile and Password
		prePage="Asserting 'Required Fields' error messages for omitted fields - Eighth Attempt - Filling Firstname, Lastname, Email, Lang Preferences, DOB, Gender, Mobile and Password";		
		a.findElement(fieldPassword).clear();
		a.findElement(fieldPassword).sendKeys("password123");
		a.findElement(buttonSubmit).click();			
		waiterText(textReqPasswordError, "");
		
		try{									
			Assert.assertEquals(a.findElement(textReqConfPassError).getText(), reqFieldMessage);
			c("Success",prePage);
		}catch (AssertionError err) {
			c(err+"",prePage);
		}

		
		
		//Asserting error message for omitted mandatory fields, and assertion of absence of error message for Firstname, lastname  and Email address, language pref, DOB, Gender, Mobile, Password and confPassword
				prePage="Asserting 'Required Fields' error messages for omitted fields - Ninth Attempt - Filling Firstname, Lastname, Email, Lang Preferences, DOB, Gender, Mobile, Password and ConfPassword";		
				a.findElement(fieldConfPassword).clear();
				a.findElement(fieldConfPassword).sendKeys("password123");			
				a.findElement(buttonSubmit).click();

				checkCurrentPage("customer/account/create/");
	}	

	public void checkElementsOnPage(){}
		
}
