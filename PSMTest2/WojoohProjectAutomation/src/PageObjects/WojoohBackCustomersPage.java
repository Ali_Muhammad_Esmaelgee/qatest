package PageObjects;

import org.junit.Assert;
import org.junit.ComparisonFailure;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.ExpectedExceptions;

import wojoohLocators.WojoohBackCustomersLocators;

public class WojoohBackCustomersPage extends WojoohBackCustomersLocators{

	public void goToCustomerAcc(String email)
	{
		a.findElement(tabCustomers).click();
		a.findElement(tabManageCustomers).click();
		a.findElement(fieldEmail).clear();
		a.findElement(fieldEmail).sendKeys(email);
		a.findElement(buttonSearch).click();		
		
		waitForRecords(textRecordsNo);								
		a.findElement(linkEdit).click();
		waiterText(tabAccInformation, "Account Information");
		a.findElement(tabAccInformation).click();
	}
				
	
	
	// waits for the records to show for 20s. breaks if total= 1 or 0.
	public void waitForRecords(By locator)
	{	
		String currText;
		int waiterStopwatch = 0;  //
		
		while(true){
			try{				
				currText = a.findElement(locator).getText();				
				
				if(currText.indexOf(" Total 0 recor") >= 0 || currText.indexOf(" Total 1 rec") >= 0){
					break;
				}
				
				Assert.assertTrue(false);
				
			}catch(AssertionError err){
				
				if(waiterStopwatch > 20){
					break;
				}
				
					waiterStopwatch++;
				
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
				// TODO Auto-generated catch block
					e.printStackTrace();
					}
				}
			}
	}
	
	
	
	
	public void checkGeneralAccount()
	{			
		try{
		Assert.assertNotEquals("BP Number Empty", "", a.findElement(fieldBpNumber).getAttribute("value"));
		Assert.assertEquals("Loyalty Enrolment Activated on General Account Registration", "0",a.findElement(dropLoyaltyEnrolment).getAttribute("value"));
		Assert.assertEquals("Loyalty Terms and Conditions on General Account Registration", "0",a.findElement(dropLoyaltyAgreeTC).getAttribute("value"));
		Assert.assertEquals("SAP/ CRM Field is not empty on General Account Registration", "",a.findElement(fieldSapId).getAttribute("value"));
		
		c("Success. Account information is Cool","This is the prepage",1);
		
		} catch(ComparisonFailure err){
			c(err+"",0);
		}
	}
	
	
	public void checkRewardAccount()
	{
		try{
		Assert.assertNotEquals("BP Number Empty is Empty on Rewards Account Registration", "", a.findElement(fieldBpNumber).getAttribute("value"));
		Assert.assertEquals("Loyalty Enrolment Activated on Rewards Account Registration", "1",a.findElement(dropLoyaltyEnrolment).getAttribute("value"));
		Assert.assertEquals("Loyalty Terms and Conditions on Rewards Account Registration", "1",a.findElement(dropLoyaltyAgreeTC).getAttribute("value"));
		Assert.assertNotEquals("SAP/ CRM Field is Empty on Rewards Account Registration", "",a.findElement(fieldSapId).getAttribute("value"));
		
		c("Success. Account information is Cool","This is the prepage",1);
		
		} catch(ComparisonFailure err){
			c(err+"",0);
		}
	}
	
	
		 
	
}
