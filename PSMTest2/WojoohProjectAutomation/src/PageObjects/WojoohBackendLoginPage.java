package PageObjects;

import wojoohLocators.WojoohBackCustomersLocators;
import wojoohLocators.WojoohBackendLoginLocators;

public class WojoohBackendLoginPage extends WojoohBackendLoginLocators{

	
	public WojoohBackendLoginPage()
	{
		login("qa.automation", "pass1234");
	}
	
	
	
	public static void login(String username, String password){
	
		String currUrl = a.getCurrentUrl();			
	if(currUrl.indexOf("admin") < 0){		
		a.get(theUrl);
	}
	
		a.findElement(fieldUsername).sendKeys(username);
		a.findElement(fieldPassword).sendKeys(password);
		a.findElement(buttonSubmit).click();
		waiterText(WojoohBackCustomersLocators.tabDashboard, "Dashboard");
	}
}
	

