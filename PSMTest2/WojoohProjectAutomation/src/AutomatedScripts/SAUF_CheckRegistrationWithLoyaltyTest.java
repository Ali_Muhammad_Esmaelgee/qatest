package AutomatedScripts;

import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

import PageObjects.MyAccountRegistrationPage;
import PageObjects.WojoohBackCustomersPage;
import PageObjects.WojoohBackendLoginPage;

public class SAUF_CheckRegistrationWithLoyaltyTest {

		WojoohFunctions func;
	
	@Test
	public void CheckRegistrationWithLoyalty() {
		func = new WojoohFunctions();
		func.setUpTarget();		
		func.createEmail();
		func.a.get(func.accessUrl);
		func.webUrl = func.webUrl.replaceAll("stag:Dubai2015@", "");		
		
		MyAccountRegistrationPage regPage = new MyAccountRegistrationPage();
		regPage.goToPage();
			
		regPage.checkMandatoryFieldsForLoyalty();
		
		regPage.checkPasswordValidation();
				
		regPage.checkDOBvalidation();
  
		regPage.goToPage();
		regPage.checkExistingEmails("loyalty");
		
		regPage.fillFormLoyalty();		
		func.a.findElement(regPage.buttonSubmit).click();
				
		new WojoohBackendLoginPage();
		WojoohBackCustomersPage backUser = new WojoohBackCustomersPage();
		backUser.goToCustomerAcc(func.email);
		backUser.checkRewardAccount();		

	}
	
	@After
	public void tearDown()
	{
		
		func.a.close();		
		func.c("Summary","Tear Down");
		
		
	}


}
