package AutomatedScripts;

import java.awt.AWTException;
import java.awt.Robot;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.Locale;

import org.apache.commons.io.FileUtils;
import org.junit.ComparisonFailure;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.HttpCommandExecutor;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import PageObjects.HomepagePage;
import PageObjects.MyAccountRegistrationPage;
import PageObjects.WojoohBackendLoginPage;
import wojoohLocators.WojoohBackCustomersLocators;
import wojoohLocators.WojoohBackendLoginLocators;
import org.openqa.selenium.support.ui.Select;
public class WojoohFunctions {

	public static String store,storeview,storePrefix,website,browser,filename,baseUrl,htaccess,email,testEmail,webUrl, accessUrl, mailSelector, platform= "";
	public static WebDriver a = null;
	public static PrintWriter writer = null;
	public static WebDriverWait waiter = null;
	
	
	public void hoverSomewhereElse()
	{
		
		try{
			String catMenu = a.findElement(By.xpath("//li[@class=\"level0 nav-2 parent menu-active opened\"]")).getText();
			
			try {
				Robot newrobot = new Robot();
				newrobot.mouseMove(0, 0);				
			} catch (AWTException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}catch (Throwable err){}

	}
	
	
	public static int setEmailAndSelectorForCurrentAcc()
	{				
		//sAKR			
		if(website.equals("S")){
			mailSelector = "1";
		}else{
			mailSelector = "2";
		}
		
		
		if(storeview.equals("ar")){
			mailSelector = mailSelector+0;
		}else{
			mailSelector = mailSelector+1;
		}
		
		
		if(store.equals("sa")){
			mailSelector = mailSelector+0;
		}else{
			mailSelector = mailSelector+1;
		}
		
		
		String thefilename = filename;
		if(thefilename.indexOf("Loyalty") < 0){
			mailSelector = mailSelector+0;
		}else{
			mailSelector = mailSelector+1;
		}
				
		 return Integer.parseInt(mailSelector);
	}
	
	
	
	public static void setUpTarget(){					

		platform = "w";
		platform = "l";
		
		createErrorFolderIfNotExists();
		
		filename = new Exception().getStackTrace()[1].getClassName();
		filename = filename.substring(filename.indexOf(".")+1);		
			
		website = filename.substring(0,1);
		storeview = filename.substring(1,2);
		store = filename.substring(2,3); 			//UAE / KSA/ worldwide
		storePrefix = store.toLowerCase(Locale.ROOT);
		browser = filename.substring(3,4);
		
		
		//setting up the website (staging or prod)
		if(website.indexOf("S") >= 0){
			htaccess = "stag:Dubai2015@wojooh.stag.redboxcloud.com";			
			
		}else if(website.indexOf("L") >= 0){
			htaccess = "wojooh.com";
		}
		
		
		baseUrl = "http://"+htaccess+"/";
		
		//setting up the storeview (Arabic or English)
		if(storeview.indexOf("A") >= 0){			
			storeview = "ar";
		}else {
			storeview = "en";
		}
		
				
		//setting up the store (Arabic or English)		
		if(store.indexOf("U") >= 0){
			store = "ae";
		}else if(store.indexOf("K") >= 0){
			store = "sa";
		}

		
		//setting up baseurl
		if(store.indexOf("W") >= 0 && storeview.indexOf("en") >= 0){
			
			webUrl = baseUrl;
		
			
		}else if(store.indexOf("W") >= 0 && storeview.indexOf("ar") >= 0){
			
			webUrl = baseUrl+storeview+"/";
			accessUrl = webUrl+"?___store=ksa_ar";
		}else{
			
			webUrl = baseUrl+store+"-"+storeview+"/";
			accessUrl = webUrl + "?___store="+storePrefix+store+"_"+storeview;
		}
		
		
		//setting up the browser
		if(browser.indexOf("F") >= 0){
//			FirefoxProfile profile = new FirefoxProfile();	
//			profile.setEnableNativeEvents(true);			
			a = new FirefoxDriver();//profile);								
		} else if(browser.indexOf("C") >= 0){
				a = new ChromeDriver();
		}else if(browser.indexOf("IE") >= 0){
			    a = new InternetExplorerDriver();			    
		}else if(browser.indexOf("S") >= 0){
				a = new SafariDriver();
		}else if(browser.indexOf("H") >= 0){
				a = new HtmlUnitDriver();
		}		
		
		
			try {
				writer = new PrintWriter(new BufferedWriter(new FileWriter("Err0r"+File.separator+filename+".html",true)));
				String title = "<strong>"+filename+"</strong><br>Date: "+(new Date()).toString()+"<br>";	            
	            writer.println("<html>\r\n<body>\r\n<head><h1><center>"+title+"</center></h1></head><p><table border=\"1\"\">");
	            writer.flush();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
			
			baseUrl = baseUrl.replaceAll("stag:Dubai2015@", "");
			
}
	
	
	
	public static void checkDropDown()
	{
			
		if(!a.findElement(By.xpath("//nav[@id=\"nav\"]/ol/li[2]")).getAttribute("class").equals("level0 nav-2 parent")){							
				((JavascriptExecutor)a).executeScript("document.getElementById('nav').childNodes[1].childNodes[2].setAttribute('class','level0 nav-2 parent')"); //change the class name and kills the drop down! 					
				checkDropDown();
		}
		
	}
	
	
	public static void waiterText(By locator, String text)
	{
		int waiterStopwatch = 0;
		
		while(true){
			try{				
				Assert.assertEquals(a.findElement(locator).getText(), text);
				break;
			}catch(AssertionError err){
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					
					if(waiterStopwatch > 20){
						break;
					}
					
						waiterStopwatch++;
				// TODO Auto-generated catch block
					e.printStackTrace();
					}
				}
			}
	}
	
	
	public static void waiterText(By locator, String text,int timeLimit)
	{
		int waiterStopwatch = 0;
		
		while(true){
			try{				
				Assert.assertEquals(a.findElement(locator).getText(), text);
				break;
			}catch(NoSuchElementException err){
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					
					if(waiterStopwatch > timeLimit){	
						c(err+"",0);
						break;		
						}
					
						waiterStopwatch++;
				// TODO Auto-generated catch block
					e.printStackTrace();
					}
				}
			}
	}
	
	
	
	
	 public static void createErrorFolderIfNotExists()
	    {
	        
		 	File theDir = new File("Err0r");
		 	if (!theDir.exists()){
		 		theDir.mkdirs();
		 	}		 	
	    }


	public static String createEmail()
	{	
		email = "RBDTest_"+System.currentTimeMillis()+"@mailinator.com";
		setEmailAndSelectorForCurrentAcc();
		return email;
		
	}

	
	public static void checkCurrentPageStupidCategoryDropdown()
	{
		try{
//				a.findElement(By.xpath("//ol[@class=\"nav-primary\"]/li[@class=\"level0 nav-2 parent menu-active opened\"]")).getAttribute("href");
			a.findElement(By.xpath("//ol[@class=\"nav-primary\"]/li[@class=\"level0 nav-2 parent menu-active opened\"]")).getText();
			System.out.println("Dropdown on");
				a.navigate().refresh();
		}catch(Throwable err){
			System.out.println("Dropdown off");
		}		
	}
	
		
	public static void goToPageByUrl(String accessMethod)
	{		
			a.get(accessMethod);		
	}
	

	
	
	public static String createCellNo()
	{
		String cellNo = System.currentTimeMillis()+"";
		return "05"+cellNo.substring(2,10);
		
	}
	
	
	public static void screenShot(long imageNo)
	{		
		File screenShotFile = ((TakesScreenshot)a).getScreenshotAs(OutputType.FILE);		
		
		try {
			FileUtils.copyFile(screenShotFile, new File("Err0r\\"+imageNo+".jpg"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	
	public static void screenShot()
	{
		
		File screenShotFile = ((TakesScreenshot)a).getScreenshotAs(OutputType.FILE);				
		try {
			FileUtils.copyFile(screenShotFile, new File("Err0rs\\ScreenShot.jpg"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}

	
	
	public static void c(String err, int errOrSuccess){
		
		long imageNo = System.currentTimeMillis();
		
		if(errOrSuccess == 0){
			
			err = err.replaceAll("<","(");
			err = err.replaceAll(">",")");
			err = "<font color=#ff0000>"+err+"</font>";
			
			String theMsg = "<strong>Current Url: </strong>"+a.getCurrentUrl()+"\r\n<br>"+err+"\"<br>\r\n";
			writer.println("<tr><td>"+theMsg+"<br><a href=\""+imageNo+".jpg\" target=\""+imageNo+"\"onclick=\"return !window.open(this.href, 'Google', 'width='+window.outerWidth / 2+',height=1000,screenX='+window.outerWidth / 2+',screenY=100')\">Screenshot: "+imageNo+"</a>\r\n<br>-----<br></td></tr>");							    	       
			writer.flush();
			
		}else if(errOrSuccess == 1){
			err = "<font color=#00ff55>"+err+"</font>";			
		}	
			
		if(platform.equals("w")){
			screenShot(imageNo);	
		}else if(platform.equals("l")){			
		}
	}
	
	
	
	public static void c(String err , String prePage, int errOrSuccess){

		long imageNo = System.currentTimeMillis();
		
		if(errOrSuccess == 0){			
			err = err.replaceAll("<","(");
			err = err.replaceAll(">",")");
			err = "<font color=#ff0000>"+err+"</font>";
			String theMsg = "<strong>Scenarios: </strong>"+prePage +": "+a.getCurrentUrl()+ "\r\n<br>"+err+"\"<br>\r\n";
			writer.println("<tr><td>"+theMsg+"<br><a href=\""+imageNo+".jpg\" target=\""+imageNo+"\"onclick=\"return !window.open(this.href, 'Google', 'width='+window.outerWidth / 2+',height=1000,screenX='+window.outerWidth / 2+',screenY=100')\">Screenshot: "+imageNo+"</a>\r\n<br>-----<br></td></tr>");							    	       
			writer.flush();
			
		}else if(errOrSuccess == 1){
			err = "<font color=#00ff55>"+err+"</font>";
			writer.println("<tr><td>"+err+"<br><a href=\""+imageNo+".jpg\" target=\""+imageNo+"\"onclick=\"return !window.open(this.href, 'Google', 'width='+window.outerWidth / 2+',height=1000,screenX='+window.outerWidth / 2+',screenY=100')\">Screenshot: "+imageNo+"</a>\r\n<br>-----<br></td></tr>");							    	       
			writer.flush();
		}	
		
		if(platform.equals("w")){
			screenShot(imageNo);	
		}else if(platform.equals("l")){			
		}		
	}
	
	
	public static void c(String err , String prePage){

		long imageNo = System.currentTimeMillis();
		
		if(err.indexOf("Success") == 0){									
			err = "<font color=#00ff55>"+err+"</font>";
			writer.println("<tr><td>"+err+": "+prePage+"<br><a href=\""+imageNo+".jpg\" target=\""+imageNo+"\"onclick=\"return !window.open(this.href, 'Google', 'width='+window.outerWidth / 2+',height=1000,screenX='+window.outerWidth / 2+',screenY=100')\">Screenshot: "+imageNo+"</a>\r\n<br>-----<br></td></tr>");							    	       
			writer.flush();			
			
		}else {			
			err = err.replaceAll("<","(");
			err = err.replaceAll(">",")");
			err = "<font color=#ff0000>"+err+"</font>";
			String theMsg = "<strong>Scenarios: </strong>"+prePage +": "+a.getCurrentUrl()+ "\r\n<br>"+err+"\"<br>\r\n";
			writer.println("<tr><td>"+theMsg+"<br><a href=\""+imageNo+".jpg\" target=\""+imageNo+"\"onclick=\"return !window.open(this.href, 'Google', 'width='+window.outerWidth / 2+',height=1000,screenX='+window.outerWidth / 2+',screenY=100')\">Screenshot: "+imageNo+"</a>\r\n<br>-----<br></td></tr>");							    	       
			writer.flush();					
		}	
		
		if(platform.equals("w")){
			screenShot(imageNo);	
		}else if(platform.equals("l")){			
		}
				
	}
	
	public static void checkCurrentPage(String restUrl)
	{
		try{			
			Assert.assertEquals(a.getCurrentUrl(), webUrl+restUrl);		
		}catch(AssertionError err){		
			System.out.println("complete url: "+webUrl+restUrl+"\n");
			a.get(webUrl+restUrl);
			checkDropDown();
			checkCurrentPage(restUrl);
		}
	}	
	
	
	public static void assertThis(By locator, String textToAssert, String desc)
	{		
		
		try{
			Assert.assertEquals(a.findElement(locator).getText(),textToAssert);
			c("Success",desc);
		}catch(AssertionError err){
			c(err+"",desc);
		}
	}
	
	}
