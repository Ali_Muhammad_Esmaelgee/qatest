package AutomatedScripts;

import static org.junit.Assert.fail;
import java.awt.AWTException;
import java.awt.Robot;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.firefox.FirefoxDriver;

import PageObjects.MyAccountRegistrationPage;
import PageObjects.WojoohBackCustomersPage;
import PageObjects.WojoohBackendLoginPage;



import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;


public class SAKF_Testing {

	WojoohFunctions func;
	StringBuffer theBuffer = new StringBuffer();
	
	@Test
	public void checkNormalRegistration() {

		
		//initialising 
		func = new WojoohFunctions();		
		func.setUpTarget();		
		func.createEmail();
		func.a.get(func.accessUrl);
		func.webUrl = func.webUrl.replaceAll("stag:Dubai2015@", "");
		
		func.a.close();
			
		MyAccountRegistrationPage regPage = new MyAccountRegistrationPage();
		regPage.goToPage();
		
		regPage.checkMandatoryFields();				
		
		regPage.checkPasswordValidation();				
						
		regPage.checkExistingEmails("general");
		
		regPage.fillFormGeneral();		
		func.a.findElement(regPage.buttonSubmit).click();
				
		//Check wrong Language selection
		
		//Check Mobile number Validation
				
//		regPage.checkConfirmationEmail("RBDTest_1447072566350@mailinator.com");
				
//		MyAccountLoginPage.login("RBDTest_1447072566350@mailinator.com", "password123");
//		MyAccountLoginPage.login(func.email, "password123");

		//Check Account Information
			//Front-end
		
			//Back-end
			new WojoohBackendLoginPage();
			WojoohBackCustomersPage backUser = new WojoohBackCustomersPage();
			backUser.goToCustomerAcc(func.email);
			backUser.checkGeneralAccount();
				
			func.writer.close();
			
		
	}
	
	
	@After
	public void tearDown()
	{
		
		func.a.close();		
		func.c("Summary","Tear Down");
		
		
	}
	
}
