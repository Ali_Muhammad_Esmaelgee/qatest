package AutomatedScripts;

import org.junit.After;
import org.junit.Test;

import PageObjects.MyAccountRegistrationPage;
import PageObjects.WojoohBackCustomersPage;
import PageObjects.WojoohBackendLoginPage;

public class SAUF_CheckNormalRegistrationTest {

	
	WojoohFunctions func;
	
	@Test
	public void CheckNormalRegistration() {

		//initialising 
		func = new WojoohFunctions();		
		func.setUpTarget();		
		func.createEmail();
		func.a.get(func.accessUrl);
		func.webUrl = func.webUrl.replaceAll("stag:Dubai2015@", "");
	
		
		MyAccountRegistrationPage regPage = new MyAccountRegistrationPage();
		regPage.goToPage();
		
		regPage.checkMandatoryFields();
		
		regPage.checkPasswordValidation();
						
		regPage.checkExistingEmails("general");
				
		regPage.fillFormGeneral();
		func.a.findElement(regPage.buttonSubmit).click();

		//Check wrong Language selection
		
		//Check Mobile number Validation
				
//		regPage.checkConfirmationEmail("RBDTest_1447072566350@mailinator.com");
				
//		MyAccountLoginPage.login("RBDTest_1447072566350@mailinator.com", "password123");
//		MyAccountLoginPage.login(func.email, "password123");

		//Check Account Information
			//Front-end
		
			//Back-end
			new WojoohBackendLoginPage();
			WojoohBackCustomersPage backUser = new WojoohBackCustomersPage();
			backUser.goToCustomerAcc(func.email);
			backUser.checkGeneralAccount();
				
			func.writer.close();

	}
	
	@After
	public void tearDown()
	{
		func.a.manage().deleteAllCookies();
		func.a.close();		
		func.c("Summary","Tear Down");
		
		
	}


}
