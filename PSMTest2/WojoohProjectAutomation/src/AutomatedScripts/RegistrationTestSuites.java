package AutomatedScripts;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;



@RunWith(Suite.class)
@SuiteClasses({ 
	
	
	SAKF_CheckNormalRegistrationTest.class,
	SEUF_CheckNormalRegistrationTest.class,
	SEKF_CheckRegistrationWithLoyaltyTest.class,
	SAKF_CheckRegistrationWithLoyaltyTest.class, 
	
	SAUF_CheckNormalRegistrationTest.class, 
	SAUF_CheckRegistrationWithLoyaltyTest.class, 
	SEKF_CheckNormalRegistrationTest.class,	
	SEUF_CheckRegistrationWithLoyaltyTest.class
	
	})

public class RegistrationTestSuites {
	
}
