/**
 * 
 */
package AutomatedScripts;

import java.io.File;
import java.io.IOException;
import java.io.Writer;

import org.apache.commons.io.FileUtils;
import org.apache.xml.utils.StopParseException;
import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.firefox.FirefoxDriver;

import PageObjects.HomepagePage;
import PageObjects.MyAccountLoginPage;
import PageObjects.MyAccountRegistrationPage;
import PageObjects.WojoohBackCustomersPage;
import PageObjects.WojoohBackendLoginPage;
import junit.framework.Assert;
import wojoohLocators.HomepageLocators;
import wojoohLocators.MyAccountRegistrationLocators;

/**
 * @author Yassar
 * Still testing period
 */
public class SEKF_CheckNormalRegistrationTest {

		WojoohFunctions func; 
	
	@Test
	public void CheckNormalRegistration() {
		
		//initialising 
		func = new WojoohFunctions();		
		func.setUpTarget();		
		func.createEmail();
		func.a.get(func.accessUrl);
		func.a.manage().window().maximize();
		
		func.webUrl = func.webUrl.replaceAll("stag:Dubai2015@", "");
		
	
		func.checkDropDown();
		
		MyAccountRegistrationPage regPage = new MyAccountRegistrationPage();
		regPage.goToPage();
		
		regPage.checkMandatoryFields();
		
		regPage.checkPasswordValidation();
						
		regPage.checkExistingEmails("general");		
		
		regPage.fillFormGeneral();		
		func.a.findElement(regPage.buttonSubmit).click();

		//Check Mobile number Validation
				
//		regPage.checkConfirmationEmail("RBDTest_1447072566350@mailinator.com");
				
//		MyAccountLoginPage.login("RBDTest_1447072566350@mailinator.com", "password123");
//		MyAccountLoginPage.login(func.email, "password123");

		//Check Account Information
			//Front-end
		
			//Back-end
			new WojoohBackendLoginPage();
			WojoohBackCustomersPage backUser = new WojoohBackCustomersPage();
			backUser.goToCustomerAcc(func.email);
			backUser.checkGeneralAccount();
				
			func.writer.close();
	}

	
	@After
	public void tearDown()
	{

		func.a.close();		
		func.c("Summary","Tear Down");
		
		
	}

}
