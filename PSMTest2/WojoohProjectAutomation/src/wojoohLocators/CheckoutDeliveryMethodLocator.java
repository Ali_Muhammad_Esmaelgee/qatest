package wojoohLocators;

import org.openqa.selenium.By;

import AutomatedScripts.WojoohFunctions;

public class CheckoutDeliveryMethodLocator extends WojoohFunctions{	  

    public static By textDeliveryMethod = By.cssSelector("#checkout-step-shipping_method > h3");
    public static By textStandardShipping = By.cssSelector("dl.sp-methods > dt");
    public static By textPrice = By.cssSelector("span.price");
    public static By textDoYouHaveAnyGiftInYourOrder = By.cssSelector("#add-gift-options-8052 > h3");
    public static By giftCheckbox = By.id("allow_gift_options");
    public static By addGiftOptionForEntireOrder = By.id("allow_gift_options_for_order");
    public static By giftMessage = By.linkText("Gift Message");
    public static By fromField = By.id("gift-message-whole-from");
    public static By toField = By.id("gift-message-whole-to");
    public static By messageField= By.id("gift-message-whole-message");
    public static By addGiftOptions = By.id("allow_gift_options_for_items");
    public static By secondGiftMessage = By.cssSelector("div.fieldset > p > a");
    public static By secondFrom = By.id("gift-message-15041-from");
    public static By secondToField = By.id("gift-message-15041-to");
    public static By secondMessageField = By.id("gift-message-15041-message");
    public static By giftReceipt = By.id("allow-gift-receipt-8052");
 
}