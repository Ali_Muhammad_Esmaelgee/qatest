package wojoohLocators;

import org.openqa.selenium.By;

public class CheckoutLoginLocator {
	
	
		public static By secureCheckout = By.cssSelector("h1 > span");
//		public static By secureCheckout2 = By.xpath("//header[@id='header']/div/div[2]/h1/span");
//		public static By secureCheckout3 = By.xpath("//h1/span");
		
		public static By loginHeader = By.cssSelector("div.step-title-inner");
//		public static By loginHeader2 = By.xpath("//dl/div/div");
//		public static By loginHeader3 = By.xpath("//div[@id='login-progress-opcheckout']/div");
		
		public static By billingHeader = By.cssSelector("dt");
//		public static By billingHeader2 = By.xpath("//div[@id='billing-progress-opcheckout']/div/dt");
//		public static By billingHeader3 = By.xpath("//dt");
		
		public static By deliveryHeader = By.cssSelector("#shipping-progress-opcheckout > div.step-title-inner > dt");
//		public static By deliveryHeader2 = By.xpath("//div[@id='shipping-progress-opcheckout']/div/dt");
//		public static By deliveryHeader3 = By.xpath("//div[3]/div/dt");
		
		public static By deliveryMethodHeader = By.cssSelector("#shipping_method-progress-opcheckout > div.step-title-inner > dt");
//		public static By deliveryMethodHeader2 = By.xpath("//div[@id='shipping_method-progress-opcheckout']/div/dt");
//		public static By deliveryMethodHeader3 = By.xpath("//div[4]/div/dt");
		
		public static By paymentHeader = By.cssSelector("#payment-progress-opcheckout > div.step-title-inner > dt");
//		public static By paymentHeader2 = By.xpath("//div[@id='payment-progress-opcheckout']/div/dt");
//		public static By paymentHeader3 = By.xpath("//div[5]/div/dt");
		
		public static By orderHeader = By.cssSelector("#review-progress-opcheckout > div.step-title-inner");
//		public static By orderHeader2 = By.xpath("//div[@id='review-progress-opcheckout']/div");
//		public static By orderHeader3 = By.xpath("//div[6]/div");
		
		public static By textAlreadyRegistered = By.cssSelector("div.col-2 > h3");
//		public static By textAlreadyRegistered2 = By.xpath("//div[@id='checkout-step-login']/div/div[2]/h3");
//		public static By textAlreadyRegistered3 = By.xpath("//div[2]/h3");
		
		public static By textNewCustomer = By.xpath("//div[@id='checkout-step-login']/div/div/h3");
//		public static By textNewCustomer2 = By.cssSelector("h3");
//		public static By textNewCustomer3 = By.xpath("//h3");
		
		public static By login = By.id("login-email");
		
		public static By password = By.id("login-password");
		
		public static By submit = By.cssSelector("div.buttons-set > button.button");
//		public static By submit2 = By.xpath("(//button[@click='submit'])[2]");
//		public static By submit3 = By.xpath("//div[@id='checkout-step-login']/div/div[2]/div/button");
//		public static By submit4 = By.xpath("//div/button");

		public static By forgotPassword = By.linkText("Forgot your password?");
//		public static By forgotPassword2 = By.cssSelector("a.f-left");
//		public static By forgotPassword3 = By.xpath("//a[contains(text(),'Forgot your password?')]");
//		public static By forgotPassword4 = By.xpath("//form[@id='login-form']/div/ul/li[3]/a");
//		public static By forgotPassword5 = By.xpath("//a[@href='http://wojooh.stag.redboxcloud.com/sa-en/customer/account/forgotpassword/']");
//		public static By forgotPassword6 = By.xpath("//li[3]/a");
//		public static By onepageCheckout = By.id("onepage-guest-register-button");	
		
}
