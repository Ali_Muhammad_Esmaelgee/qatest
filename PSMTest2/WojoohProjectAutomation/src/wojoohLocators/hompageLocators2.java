package wojoohLocators;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class hompageLocators2 {

	public static int i=1;
	public static int j=1;
	
	public static By SectionXpath = By.xpath("//div[@class=\"std\"]/div["+i+"]//div[@class=\"widget-title\"]//span");
	public static By SectionCss = By.cssSelector("div.std > div:nth-child(i) div.widget-title span");
		
	public static By imageXpath = By.xpath("//div[@class=\"std\"]/div["+i+"]//div[@class=\"owl-item active\"]["+j+"]//a[@class=\"product-image\"] , where j is no of item");
	public static By imageCss = By.cssSelector("div.std > div:nth-child(i) div.owl-item.active:nth-child(j)");
		
	public static By stickers = By.xpath("//div[@class=\"std\"]/div["+i+"]//div[@class=\"owl-item active\"]["+j+"]//span[@class=\"sticker exclusive\"]/span");
		
		
	public static By brandXpath = By.xpath("//div[@class=\"std\"]/div["+i+"]//div[@class=\"owl-item active\"]["+j+"]//h4[@class=\"brand-name\"]");
	public static By brandCss = By.cssSelector("div.std > div:nth-child(3) div.owl-item.active:nth-child(3) h4.brand-name");
		
	public static By nameXpath = By.xpath("//div[@class=\"std\"]/div["+i+"]//div[@class=\"owl-item active\"]["+j+"]//h3[@class=\"product-name\"]/a");
	public static By nameCss = By.cssSelector("div.std > div:nth-child(3) div.owl-item.active:nth-child(3) h3.product-name a");
		
	public static By priceXpath = By.xpath("//div[@class=\"std\"]/div["+i+"]//div[@class=\"owl-item active\"]["+j+"]//span[@class=\"price\"]");
	public static By priceCss = By.cssSelector("div.std > div:nth-child(3) div.owl-item.active:nth-child(3) span.price");
		
		
	public static By ourRecommendationsSection = By.xpath("//div[@class=\"std\"]/div["+i+"]//div[@class=\"owl-item active\"]["+j+"]");
		
	public static By recommendationsImageXpath = By.xpath("//div[@class=\"std\"]/div["+i+"]//div[@class=\"owl-item active\"]["+j+"]//a[@class=\"product-image\"] i: section, j: product number");
	public static By recommendationsImageCss = By.cssSelector("div.std > div:nth-child(9) div.owl-item.active:nth-child(3) a.product-image");
		
	public static By sticker = By.xpath("//div[@class=\"std\"]/div["+i+"]//div[@class=\"owl-item active\"]["+j+"]//span[@class=\"sticker exclusive\"]/span");
		
	public static By recommendationBrandXpath = By.xpath("//div[@class=\"std\"]/div[9]//div[@class=\"owl-item active\"][1]//h4[@class=\"brand-name\"]");
	public static By recommendationsBrandCss = By.cssSelector("div.std > div:nth-child(9) div.owl-item.active:nth-child(3) h4.brand-name");
		
	public static By recommendationsnameXpath = By.xpath("//div[@class=\"std\"]/div["+i+"]//div[@class=\"owl-item active\"]["+j+"]//h3[@class=\"product-name\"]/a");
	public static By recommendationsnameCss = By.cssSelector("div.std > div:nth-child(9) div.owl-item.active:nth-child(3) h3.product-name a");
		
	public static By recommendationsPriceXpath = By.xpath("//div[@class=\"std\"]/div["+i+"]//div[@class=\"owl-item active\"]["+j+"]//span[@class=\"price\"]");
	public static By recommendationspriceCss = By.cssSelector("div.std > div:nth-child(9) div.owl-item.active:nth-child(3) span.price");
}