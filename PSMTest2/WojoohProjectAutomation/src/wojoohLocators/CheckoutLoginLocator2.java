package wojoohLocators;

import org.openqa.selenium.By;

public class CheckoutLoginLocator2 {

	
	String storeview;
	
	public CheckoutLoginLocator2(){};
	
	public CheckoutLoginLocator2(String storeview){
		
		By secureCheckout, loginHeader;
		
		if(storeview.indexOf("ar") >= 0){
			
			secureCheckout = By.xpath("English");
			loginHeader = By.xpath("English");
			
		}else {
			secureCheckout = By.xpath("Arabic");
			loginHeader = By.xpath("Arabic");			
		}
		
	}
	
}