package wojoohLocators;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

import AutomatedScripts.WojoohFunctions;

public class MyAccountRegistrationLocators extends WojoohFunctions{
    	
	
	public static String theUrl = webUrl + "customer/account/create"; 
	
			
	public static By fieldFirstname = By.id("firstname");
	public static By fieldLastname = By.id("lastname");    
	public static By fieldEmail = By.id("email_address");    
	public static By radioLangSelectorAr = By.id("pref_lang-103");
	public static By radioLangSelectorEng = By.id("pref_lang-102");
	public static By checkboxLoyaltyEnrolment = By.id("reward_enrolment");
	public static By fieldDay = By.id("day");
	public static By fieldMonth = By.id("month");
	public static By fieldYear = By.id("year");
	public static By radioMale = By.id("gender-1");
	public static By radoiFemale = By.id("gender-2");
	public static By fieldCellphone = By.id("cellphone");
	public static By checkboxTermsandconditions = By.id("reward_agree_tc");
	public static By fieldPassword = By.id("password");
	public static By fieldConfPassword = By.id("confirmation");
	public static By checkboxNewsletter = By.id("is_subscribed");
	public static By textReqFirstname = By.id("advice-required-entry-firstname");
	public static By textReqLastname = By.id("advice-required-entry-lastname");
	public static By textReqEmail = By.id("advice-required-entry-email_address");
	public static By textReqLangSelector = By.id("advice-validate-one-required-pref_lang-103");
	public static By textReqDateValidFullDate = By.xpath("//div[5]");
	public static By textReqGender = By.id("advice-validate-one-required-gender-1");
	public static By textReqCellphone  = By.id("advice-required-entry-cellphone");
	public static By textReqPasswordError = By.id("advice-required-entry-password");
	public static By textReqConfPassError = By.id("advice-required-entry-confirmation");	
	public static By textPasswordError = By.id("advice-validate-password-password");
	public static By textConfPassError = By.id("advice-validate-cpassword-confirmation");
	public static By textHeaderError = By.cssSelector("li > span");
	public static By buttonSubmit = By.xpath("//div[2]/button");
	public static By mailinatorEmail = By.cssSelector("div.subject.ng-binding");
	public static By linkAccValidation = By.cssSelector("font > font > a");
	
	public MyAccountRegistrationLocators()
	{
		
		
		
		if(storeview.indexOf("en") >= 0){
			setEnglishLocators();
		}else if(storeview.indexOf("ar") >= 0){
			setArabicLocators();
		}		
	}
			
	
	public void setArabicLocators(){
	}
	
	
	
	public void setEnglishLocators(){
	}
	
	
	
    	
    
}

