package wojoohLocators;

import org.openqa.selenium.By;

import AutomatedScripts.WojoohFunctions;


public class WojoohBackCustomersLocators extends WojoohFunctions{
    	
    	public static By tabDashboard = By.xpath("//ul[@id=\"nav\"]/li[1]/a");
    	public static By tabCustomers = By.xpath("//ul[@id=\"nav\"]/li[4]/a"); 
    	public static By tabManageCustomers = By.xpath("//ul[@id=\"nav\"]/li[4]/ul/li/a/span");
    	public static By fieldEmail = By.id("customerGrid_filter_email");
    	public static By buttonSearch = By.xpath("//div[@id=\"customerGrid\"]/table/tbody/tr/td[3]/button[2]");
    	public static By buttonSearch2 = By.xpath("//button[2]");
    	public static By linkEdit = By.xpath("//td[12]/a");
    	public static By tabAccInformation = By.cssSelector("#customer_info_tabs_account > span"); // Account information tab in Customer Information layered navigation 
    	public static By fieldBpNumber = By.id("_accountbp_number");
    	public static By dropLoyaltyEnrolment = By.id("_accountreward_enrolment");
    	public static By dropLoyaltyAgreeTC = By.id("_accountreward_agree_tc");
    	public static By fieldSapId = By.id("_accountreward_member_id");
    	public static By textRecordsNo = By.cssSelector("td.pager");    	
    	public static By textRecords = By.xpath("//div[2]/div/table/tbody/tr/td");

}


