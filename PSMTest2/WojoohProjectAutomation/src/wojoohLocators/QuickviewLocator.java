package wojoohLocators;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class QuickviewLocator {
	public static By imageZoom = By.cssSelector("div.zoomWindow");
	public static By imageZoom2 = By.xpath("//html[@id='top']/body/div[3]/div/div");
	public static By imageZoom3 = By.xpath("//div[3]/div/div");
	public static By brand = By.cssSelector("h2");
	public static By brand2 = By.xpath("//form[@id='product_addtocart_form']/div[2]/div/h2");
	public static By brand3 = By.xpath("//h2");
	public static By productName = By.cssSelector("h1");
	public static By productName2 = By.xpath("//form[@id='product_addtocart_form']/div[2]/div[2]/h1");
	public static By productName3 = By.xpath("//h1");
	public static By price = By.cssSelector("span.price");
	public static By price2 = By.xpath("//span[@id='product-price-115']/span");
	public static By price3 = By.xpath("//span/span");
	public static By stockStatus = By.cssSelector("span.value");
	public static By stockStatus2 = By.xpath("//form[@id='product_addtocart_form']/div[2]/p/span[2]");
	public static By stockStatus3 = By.xpath("//span[2]");
	public static By qty = By.id("qty");
	public static By addToCart = By.cssSelector("button.button.btn-cart");
	public static By addToCart2 = By.xpath("//button[@type='button']");
	public static By addToCart3 = By.xpath("//form[@id='product_addtocart_form']/div[2]/div[4]/div/div[2]/button");
	public static By addToCart4 = By.xpath("//button");
	public static By addToWishlist = By.cssSelector("a.button.link-wishlist > span > span");
	public static By addToWishlist2 = By.xpath("//form[@id='product_addtocart_form']/div[2]/div[4]/ul/li/a/span/span");
	public static By addToWishlist3 = By.xpath("//a/span/span");
	public static By ViewFullDetails = By.linkText("View Full Details");
	public static By viewFullDetails2 = By.cssSelector("a.view-full");
	public static By viewFullDetails3 = By.xpath("//a[contains(text(),'View Full Details')]");
	public static By viewFullDetails4 = By.xpath("//form[@id='product_addtocart_form']/div[2]/div[4]/ul/li[2]/a");
	public static By viewFullDetails5 = By.xpath("//a[@href='http://wojooh.stag.redboxcloud.com/sa-en/lip-mst-stk-pink-wojooh']");
	public static By viewFullDetails6 = By.xpath("//li[2]/a");
}