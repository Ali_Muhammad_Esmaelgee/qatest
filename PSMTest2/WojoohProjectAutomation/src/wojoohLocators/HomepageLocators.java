package wojoohLocators;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

import AutomatedScripts.WojoohFunctions;

public class HomepageLocators extends WojoohFunctions{

	
	
	
	
		public static By dropMyAccount = By.xpath("//div[@class=\"page-header-container\"]/div/div/div/a/span");
		public static By linkLogin = By.xpath("//div[@id=\"header-account\"]//li[2]");
		public static By linkRegister  = By.xpath("//div[@id=\"header-account\"]//li[1]");
		public static By imWojoohLogo  = By.cssSelector("img.large");
		public static By linkMiniCart  = By.cssSelector("span.price");
		public static By fieldSearch   = By.id("search");
		public static By textBestsellers   = By.cssSelector("h2 &gt; span");
		public static By linkCarouselImageHim  = By.xpath("//picture/div/a/img");
		public static By linkCarouselImageHer  = By.xpath("//div[2]/a/img");
		public static By textFBrand = By.xpath("//html[@id='top']/body/div/div/div[2]/div/div/div/div[4]/div[3]/div/div[2]/div/div/div/div/span/div[3]/h4");
		public static By textLBrand = By.xpath("//html[@id='top']/body/div/div/div[2]/div/div/div/div[5]/div[2]/div/div/div/div/div/div/span/div[3]/h4");
		public static By textSBrand = By.xpath("(//a[contains(text(),'Quick View')])[7]");
		public static By linkFQuickview = By.xpath("(//a[contains(text(),'Quick View')])[10]");
		public static By linkLQuickview = By.xpath("(//a[contains(text(),'Quick View')])[16]");
		public static By linkSquickview = By.xpath("//html[@id='top']/body/div/div/div[2]/div/div/div/div[6]/div[3]/div/div/div/div/div/div/span/div[3]/h4");
		public static By fieldNewsletterName = By.id("firstname");
		public static By fieldNewsletterEmail  = By.id("newsletter");
		public static By buttonFemaleOption = By.name("F");
		public static By buttonMaleOption  = By.name("M");
		public static By textCopyrightBottom = By.cssSelector("span.none");
		public static By imFImage  = By.xpath("//div[3]/div/div[2]/div/div/div/div/span/div/a/img");
		public static By linkFName = By.xpath("//div[3]/div/div[2]/div/div/div/div/span/div[3]/h3/a");
		public static By textFPrice = By.xpath("//span[@id='product-price-465-widget-new-grid']/span"); 
		public static By textLatestMakeupSection   = By.xpath("//html[@id='top']/body/div/div/div[2]/div/div/div/div[5]/div/h2/span");
		public static By linkStoreviewEngSwitch = By.linkText("English");
		public static By linkBeautyStores  = By.linkText("Our Beauty Stores");
		public static By linkBeautyShop = By.linkText("Beauty Shop");
		public static By linkBeautyBrands  = By.linkText("Beauty Brands");
		public static By linkWhatsNew  = By.linkText("What's New");
		public static By linkBeautyBlog = By.linkText("Beauty Blog");
		public static By linkBeautyRewards = By.linkText("Beauty Rewards");
		public static By imCarouselImage   = By.xpath("//img[contains(@src,'http://wojooh.stag.redboxcloud.com/media/wysiwyg/banners/homepage-banner-desktop-half-length-hugo-boss_1.jpg')]");
		public static By imLImage  = By.xpath("//div[2]/div/div/div/div/div/div/span/div/a/img"); 
		public static By linkLName = By.xpath("//div[2]/div/div/div/div/div/div/span/div[3]/h3/a");
		public static By linkLPrice = By.xpath("//div[2]/div/div/div/div/div/div/span/div[3]/a/div/span/span"); 
		public static By imSImage  = By.xpath("//div[6]/div[3]/div/div/div/div/div/div/span/div/a/img"); 
		public static By linkSName = By.cssSelector("a[title=\"Age Defense BB Cream Broad  Spectrum SPF 30 (Shade 02))\"]");
		public static By textSPrice = By.cssSelector("#product-price-726-widget-new-grid > span.price");
		public static By linkAboutUS   = By.linkText("About us");
		public static By linkDeliveryReturns   = By.linkText("Delivery & returns");
		public static By linkFaqs = By.linkText("FAQ's");
		public static By linkTermsConditions   = By.linkText("Terms & Conditions");
		public static By linkPrivacyPolicy = By.linkText("Privacy Policy");
		public static By linkSitemap   = By.linkText("Sitemap");
		public static By linkContactUs = By.linkText("Contact us");
		public static By linkMoveFocusUp   = By.cssSelector("span.fa.fa-chevron-up");
 
}