package wojoohLocators;
import org.openqa.selenium.By;

import AutomatedScripts.WojoohFunctions;

public class MyAccountLoginLocator extends WojoohFunctions{
  	
		public static String theUrl = baseUrl + "customer/account/login/";		
		public static By fieldUsername = By.id("email");
		public static By fieldPass = By.id("pass");
		public static By buttonSubmit = By.id("send2");
		public static By buttonCreateAccount = By.xpath("//div[2]/a/span/span");
		public static By headerAlreadyRegistered = By.xpath("//div[2]/div/h2");
		public static By headerLoginPage = By.cssSelector("h1");
		public static By headerMessage = By.xpath("//li/span");
		public static By linkForgetPassword = By.xpath("//div/ul/li[3]/a");
		
		
	public MyAccountLoginLocator(){	
		if(storeview.indexOf("en") >= 0){
			setEnglishLocators();
		}else if(storeview.indexOf("ar") >= 0){
			setArabicLocators();
		}		
	}
			
	
	public void setArabicLocators(){
	}
	
	
	
	public void setEnglishLocators(){
	}
	

}