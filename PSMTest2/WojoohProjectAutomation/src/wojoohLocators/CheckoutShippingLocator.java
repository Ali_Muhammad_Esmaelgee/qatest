package wojoohLocators;

import org.openqa.selenium.By;

public class CheckoutShippingLocator {

		public static By useBillingAddressCheckbox = By.id("shipping:same_as_billing");
		public static By name = By.id("shipping:firstname");
		public static By thelastname = By.id("shipping:lastname");
		public static By theaddress = By.id("shipping:street1");
		public static By the2address = By.id("shipping:street2");
		public static By city = By.id("shipping:city_id");
		public static By country = By.id("shipping:country_id");
		public static By telephone = By.id("shipping:telephone");
		public static By fax = By.id("shipping:fax");
  
}
