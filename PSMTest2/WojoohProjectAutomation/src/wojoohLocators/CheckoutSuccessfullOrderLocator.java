package wojoohLocators;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class CheckoutSuccessfullOrderLocator {

		
		public static By textThankYouMessage = By.cssSelector("h1");
		public static By textThankYouMessage2 = By.xpath("//html[@id='top']/body/div/div/div/div/div/div/h1");
		public static By textThankYouMessage3 = By.xpath("//h1");
		public static By textEmailNotifierMessage = By.cssSelector("p.order-text");
		public static By textEmailNotifierMessage2 = By.xpath("//html[@id='top']/body/div/div/div/div/div/p");
		public static By textEmailNotifierMessage3 = By.xpath("//body/div/div/div/div/div/p");
		public static By phoneNo = By.cssSelector("span.skype_c2c_text_span");
		public static By phoneNo2 = By.xpath("//span[@id='free_num_ui']/span");
		public static By phoneNo3 = By.xpath("//span[2]/span/span/span");
		public static By callFree = By.cssSelector("span.skype_c2c_free_text_span");
		public static By callFree2 = By.xpath("//span[@id='free_num_ui']/span[2]");
		public static By callFree3 = By.xpath("//span/span[2]");
		public static By orderNo = By.cssSelector("p.order-number > strong > a");
		public static By orderNo2 = By.xpath("//html[@id='top']/body/div/div/div/div/div/p[2]/strong/a");
		public static By orderNo3 = By.xpath("//a[@href='http://wojooh.stag.redboxcloud.com/sa-en/sales/order/view/order_id/3042/']");
		public static By orderNo4 = By.xpath("//p[2]/strong/a");
		public static By shareOnTwitter = By.linkText("Share on Twitter");
		public static By shareOnFB = By.linkText("Share on Facebook");
		public static By continueShopping = By.cssSelector("button.button2");
		public static By goToMyAccount = By.linkText("Go To My Account");



}