/**
 * 
 */
package PageObjects;

import org.openqa.selenium.WebDriver;

import WojoohMainFunctions.WojoohFunctions;
import wojoohLocators.HomepageLocators;
import wojoohLocators.MyAccountRegistrationLocators;

/**
 * @author Yassar
 *
 */
public class MyAccountRegistrationPage extends MyAccountRegistrationLocators{

	//implement following	
	//2. create assertionMethod
	
	public void fillForm(){				
		a.findElement(fieldFirstname).sendKeys("Jacks");
		a.findElement(fieldLastname).sendKeys("Stone");
		a.findElement(fieldEmail).sendKeys(createEmail());
		a.findElement(fieldPassword).sendKeys("password123");
		a.findElement(fieldConfPassword).sendKeys("password123");	
	}
    
		
	
	public void goToPage()
	{		
			a.findElement(HomepageLocators.dropMyAccount).click();
			a.findElement(HomepageLocators.linkRegister).click();		
	}
	
	
	
	public void checkMandatoryFields(){}
	public void checkExistingEmails(){}
	public void checkPasswordValidation(){}
	public void checkDOBvalidation(){}
	public void checkElementsOnPage(){}
	
	
	public void checkEmail()
	{
		email = "jackstones00565460545@mailnator.com"; //codat
		String emailName = email; 
		emailName = email.substring(0,emailName.indexOf("@")); 
		
		a.get("https://mailinator.com/inbox.jsp?to="+emailName);		
	}
	
	
	
	
	
	
	
	
	
	
}
