package PageObjects;

import WojoohMainFunctions.WojoohFunctions;
import wojoohLocators.HomepageLocators;
import wojoohLocators.MyAccountLoginLocator;

public class MyAccountLoginPage extends MyAccountLoginLocator {
	
	
	
	
	public void login(String usernames, String password){		
		a.findElement(fieldUsername).sendKeys(usernames);
		a.findElement(fieldPass).sendKeys(password);
		a.findElement(buttonSubmit).click();		
	}
	
	
	
	
	public void goToMyAccountLoginPage(){	
			a.get(theUrl);
	}
	
	
	public void goToMyAccountLoginPage(String methodOfAccess){	
			if(methodOfAccess.indexOf("MIMIC_USER") >= 0){
				a.findElement(HomepageLocators.dropMyAccount).click();
				a.findElement(HomepageLocators.linkLogin).click();
			}
	}
	
}
