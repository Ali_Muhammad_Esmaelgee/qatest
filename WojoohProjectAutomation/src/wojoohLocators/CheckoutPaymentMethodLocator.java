package wojoohLocators;

import org.openqa.selenium.By;

public interface CheckoutPaymentMethodLocator{
	  public static By textHowWouldLikeToPay = By.cssSelector("#checkout-step-payment > h3");
	  public static By textHowWouldLikeToPay2 = By.xpath("//div[@id='checkout-step-payment']/h3");
	  public static By textHowWouldLikeToPay3 = By.xpath("//li[4]/div[2]/h3");
	  public static By creditCardRadio = By.id("p_method_payfortcw_creditcard");
	  public static By creditCardRadio2 = By.name("payment[method]");
	  public static By creditCardRadio3 = By.cssSelector("#p_method_payfortcw_creditcard");
	  public static By creditCardRadio4 = By.xpath("//input[@id='p_method_payfortcw_creditcard']");
	  public static By creditCardRadio5 = By.xpath("//dt[@id='dt_method_payfortcw_creditcard']/input");
	  public static By creditCardRadio6 = By.xpath("//dt/input");
	  public static By cardName = By.id("7741d9f4-50fd-48e6-9a9f-cf084c22553e");
	  public static By cardName2 = By.name("payfortcw_creditcard[CN]");
	  public static By cardName3 = By.cssSelector("#7741d9f4-50fd-48e6-9a9f-cf084c22553e");
	  public static By cardName4 = By.xpath("//input[@id='7741d9f4-50fd-48e6-9a9f-cf084c22553e']");
	  public static By cardName5 = By.xpath("//div[@id='7741d9f4-50fd-48e6-9a9f-cf084c22553e-wrapper']/input");
	  public static By cardName6 = By.xpath("//div[2]/li/div/input");
	  public static By cardNumber = By.id("cb8cd4fa-3e95-4d6a-af6b-d242ffcbc36f");
	  public static By cardNumber2 = By.cssSelector("#cb8cd4fa-3e95-4d6a-af6b-d242ffcbc36f");
	  public static By cardNumber3 = By.xpath("//input[@id='cb8cd4fa-3e95-4d6a-af6b-d242ffcbc36f']");
	  public static By cardNumber4 = By.xpath("//div[@id='cb8cd4fa-3e95-4d6a-af6b-d242ffcbc36f-wrapper']/input");
	  public static By cardNumber5 = By.xpath("//li[2]/div/div/div/input");
	  public static By month = By.id("337d6321-092d-4005-a687-a8210674ffd4");
	  public static By month2 = By.name("payfortcw_creditcard[ECOM_CARDINFO_EXPDATE_MONTH]");
	  public static By month3 = By.cssSelector("#337d6321-092d-4005-a687-a8210674ffd4");
	  public static By month4 = By.xpath("//type[@id='337d6321-092d-4005-a687-a8210674ffd4']");
	  public static By month5 = By.xpath("//div[@id='337d6321-092d-4005-a687-a8210674ffd4-wrapper']/type");
	  public static By month6 = By.xpath("//div/div/div/type");
	  public static By year = By.id("9ba8b087-c991-4ea8-8956-b00ad6736b9a");
	  public static By year2 = By.name("payfortcw_creditcard[ECOM_CARDINFO_EXPDATE_YEAR]");
	  public static By year3 = By.cssSelector("#9ba8b087-c991-4ea8-8956-b00ad6736b9a");
	  public static By year4 = By.xpath("//type[@id='9ba8b087-c991-4ea8-8956-b00ad6736b9a']");
	  public static By year5 = By.xpath("//div[@id='9ba8b087-c991-4ea8-8956-b00ad6736b9a-wrapper']/type");
	  public static By year6 = By.xpath("//div[2]/type");
	  public static By cvc = By.id("8899fc7a-6398-4440-823a-b45b350accf1");
	  public static By cvc2 = By.name("payfortcw_creditcard[CVC]");
	  public static By cvc3 = By.cssSelector("#8899fc7a-6398-4440-823a-b45b350accf1");
	  public static By cvc4 = By.xpath("//input[@id='8899fc7a-6398-4440-823a-b45b350accf1']");
	  public static By cvc5 = By.xpath("//div[@id='8899fc7a-6398-4440-823a-b45b350accf1-wrapper']/input");
	  public static By cvc6 = By.xpath("//div[2]/li[4]/div/input");
	  public static By cod = By.id("p_method_cashondelivery");
	  public static By submit = By.cssSelector("#payment-buttons-container > button.button");
	  public static By submit2 = By.xpath("(//button[@type='button'])[4]");
	  public static By submit3 = By.xpath("//div[@id='payment-buttons-container']/button");
	  public static By submit4 = By.xpath("//div[2]/button");

  }
  