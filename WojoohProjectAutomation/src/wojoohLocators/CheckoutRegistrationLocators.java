package wojoohLocators;

import org.openqa.selenium.By;

public interface CheckoutRegistrationLocators{
	
	public static By firstname = By.id("billing:firstname");
	public static By lastname = By.id("billing:lastname");
	public static By email = By.id("billing:email");
	public static By address = By.id("billing:street1");
	public static By streetaddress2 = By.id("billing:street2");
	public static By city = By.id("billing:city_id");
	public static By country = By.id("billing:country_id");
	public static By telephone = By.id("billing:telephone");
	public static By fax = By.id("billing:fax");
	public static By english = By.id("billing:pref_lang-102");
	public static By Arabic = By.id("billing:pref_lang-103");
	public static By loyaltyCheckbox = By.id("billing:reward_enrolment");
	public static By day = By.id("billing:day");
	public static By month = By.id("billing:month");
	public static By year = By.id("billing:year");
	public static By male = By.id("billing:gender-1");
	public static By female = By.id("billing:gender-2");
	public static By cellphone = By.id("billing:cellphone");
	public static By termsAndConditions = By.id("billing:reward_agree_tc");
	public static By password = By.id("billing:customer_password");
	public static By confPassword = By.id("billing:confirm_password");
	public static By useDifferentAddRadio = By.id("billing:use_for_shipping_no");
	public static By useSameAddRadio = By.id("billing:use_for_shipping_yes");
	public static By submit= By.cssSelector("#billing-buttons-container > button.button");
	public static By submit2 = By.xpath("//button[@type='button']");
	public static By submit3 = By.xpath("//div[@id='billing-buttons-container']/button");
	public static By submit4 = By.xpath("//form/div/div/button");
  
}