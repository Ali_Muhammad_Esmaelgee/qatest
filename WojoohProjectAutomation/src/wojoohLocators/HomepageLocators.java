package wojoohLocators;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

import WojoohMainFunctions.WojoohFunctions;

public class HomepageLocators extends WojoohFunctions{

	
	public static By dropMyAccount,linkLogin,linkRegister,imWojoohLogo,linkMiniCart,fieldSearch,textBestsellers,linkCarouselImageHim,linkCarouselImageHer,textFBrand,textLBrand,textSBrand,linkFQuickview,linkLQuickview,linkSquickview,fieldNewsletterName,fieldNewsletterEmail,buttonFemaleOption,buttonMaleOption,textCopyrightBottom,imFImage,linkFName,textFPrice,textLatestMakeupSection,linkStoreviewEngSwitch,linkBeautyStores,linkBeautyShop,linkBeautyBrands,linkWhatsNew,linkBeautyBlog,linkBeautyRewards,imCarouselImage,imLImage,linkLName,linkLPrice,imSImage,linkSName,textSPrice,linkAboutUS,linkDeliveryReturns,linkFaqs,linkTermsConditions,linkPrivacyPolicy,linkSitemap,linkContactUs,linkMoveFocusUp;
	
	public HomepageLocators(){		
		dropMyAccount = By.xpath("//div[2]/div/div/a");
		linkLogin = By.xpath("//li[2]/a");
		linkRegister  = By.xpath("//li/a");
		imWojoohLogo  = By.cssSelector("img.large");
		linkMiniCart  = By.cssSelector("span.price");
		fieldSearch   = By.id("search");
		textBestsellers   = By.cssSelector("h2 &gt; span");
		linkCarouselImageHim  = By.xpath("//picture/div/a/img");
		linkCarouselImageHer  = By.xpath("//div[2]/a/img");
		textFBrand = By.xpath("//html[@id='top']/body/div/div/div[2]/div/div/div/div[4]/div[3]/div/div[2]/div/div/div/div/span/div[3]/h4");
		textLBrand = By.xpath("//html[@id='top']/body/div/div/div[2]/div/div/div/div[5]/div[2]/div/div/div/div/div/div/span/div[3]/h4");
		textSBrand = By.xpath("(//a[contains(text(),'Quick View')])[7]");
		linkFQuickview = By.xpath("(//a[contains(text(),'Quick View')])[10]");
		linkLQuickview = By.xpath("(//a[contains(text(),'Quick View')])[16]");
		linkSquickview = By.xpath("//html[@id='top']/body/div/div/div[2]/div/div/div/div[6]/div[3]/div/div/div/div/div/div/span/div[3]/h4");
		fieldNewsletterName = By.id("firstname");
		fieldNewsletterEmail  = By.id("newsletter");
		buttonFemaleOption = By.name("F");
		buttonMaleOption  = By.name("M");
		textCopyrightBottom = By.cssSelector("span.none");
		imFImage  = By.xpath("//div[3]/div/div[2]/div/div/div/div/span/div/a/img");
		linkFName = By.xpath("//div[3]/div/div[2]/div/div/div/div/span/div[3]/h3/a");
		textFPrice = By.xpath("//span[@id='product-price-465-widget-new-grid']/span"); 
		textLatestMakeupSection   = By.xpath("//html[@id='top']/body/div/div/div[2]/div/div/div/div[5]/div/h2/span");
		linkStoreviewEngSwitch = By.linkText("English");
		linkBeautyStores  = By.linkText("Our Beauty Stores");
		linkBeautyShop = By.linkText("Beauty Shop");
		linkBeautyBrands  = By.linkText("Beauty Brands");
		linkWhatsNew  = By.linkText("What's New");
		linkBeautyBlog = By.linkText("Beauty Blog");
		linkBeautyRewards = By.linkText("Beauty Rewards");
		imCarouselImage   = By.xpath("//img[contains(@src,'http://wojooh.stag.redboxcloud.com/media/wysiwyg/banners/homepage-banner-desktop-half-length-hugo-boss_1.jpg')]");
		imLImage  = By.xpath("//div[2]/div/div/div/div/div/div/span/div/a/img"); 
		linkLName = By.xpath("//div[2]/div/div/div/div/div/div/span/div[3]/h3/a");
		linkLPrice = By.xpath("//div[2]/div/div/div/div/div/div/span/div[3]/a/div/span/span"); 
		imSImage  = By.xpath("//div[6]/div[3]/div/div/div/div/div/div/span/div/a/img"); 
		linkSName = By.cssSelector("a[title=\"Age Defense BB Cream Broad  Spectrum SPF 30 (Shade 02))\"]");
		textSPrice = By.cssSelector("#product-price-726-widget-new-grid > span.price");
		linkAboutUS   = By.linkText("About us");
		linkDeliveryReturns   = By.linkText("Delivery & returns");
		linkFaqs  = By.linkText("FAQ's");
		linkTermsConditions   = By.linkText("Terms & Conditions");
		linkPrivacyPolicy = By.linkText("Privacy Policy");
		linkSitemap   = By.linkText("Sitemap");
		linkContactUs = By.linkText("Contact us");
		linkMoveFocusUp   = By.cssSelector("span.fa.fa-chevron-up");
	}
}