package wojoohLocators;

import org.openqa.selenium.By;

public interface CheckoutOrderReviewLocator {
	
	public static By OrderReview = By.cssSelector("#checkout-step-review > h3");
	public static By OrderReview2 = By.xpath("//div[@id='checkout-step-review']/h3");
	public static By OrderReview3 = By.xpath("//li[5]/div[2]/h3");
	public static By productName = By.cssSelector("h3.product-name");
	public static By productName2 = By.xpath("//table[@id='checkout-review-table']/tbody/tr/td/h3");
	public static By productName3 = By.xpath("//td/h3");
	public static By productPrice = By.cssSelector("span.cart-price > span.price");
	public static By productPrice2 = By.xpath("//table[@id='checkout-review-table']/tbody/tr/td[2]/span/span");
	public static By productPrice3 = By.xpath("//td[2]/span/span");
	public static By productQty = By.cssSelector("td.a-center");
	public static By productQty2 = By.xpath("//table[@id='checkout-review-table']/tbody/tr/td[3]");
	public static By productQty3 = By.xpath("//td[3]");
	public static By productSubtotal = By.cssSelector("td.a-right.last > span.cart-price > span.price");
	public static By productSubtotal2 = By.xpath("//table[@id='checkout-review-table']/tbody/tr/td[4]/span/span");
	public static By productSubtotal3 = By.xpath("//td[4]/span/span");
	public static By secondSubtotal = By.cssSelector("td.a-right.last > span.price");
	public static By secondSubtotal2 = By.xpath("//table[@id='checkout-review-table']/tfoot/tr/td[2]/span");
	public static By secondSubtotal3 = By.xpath("//td[2]/span");
	public static By shippingTotal = By.xpath("//table[@id='checkout-review-table']/tfoot/tr[2]/td[2]/span");
	public static By shippingTotal2 = By.xpath("//tr[2]/td[2]/span");
	public static By codSurcharge = By.xpath("//table[@id='checkout-review-table']/tfoot/tr[3]/td[2]/span");
	public static By codSurcharge2 = By.xpath("//tr[3]/td[2]/span");
	public static By total = By.cssSelector("strong > span.price");
	public static By total2 = By.xpath("//table[@id='checkout-review-table']/tfoot/tr[4]/td[2]/strong/span");
	public static By total3 = By.xpath("//strong/span");
	public static By textTerms = By.xpath("//form[@id='checkout-agreements']/ol/li/div/p[4]");
	public static By textTerms2 = By.xpath("//p[4]");
	public static By termsCheckbot = By.id("agreement-1");
	public static By checkout = By.cssSelector("button.button.btn-checkout");
	public static By checkout2 = By.xpath("//button[@type='submit']");
	public static By checkout3 = By.xpath("//div[@id='review-buttons-container']/button");
	public static By checkout4 = By.xpath("//div/div[2]/div/button");
}