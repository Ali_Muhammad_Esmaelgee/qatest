package wojoohLocators;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

import WojoohMainFunctions.WojoohFunctions;

public class MyAccountRegistrationLocators extends WojoohFunctions{
    	
	public static By fieldFirstname,fieldLastname,fieldEmail,radioLangSelectorAr,radioLangSelectorEng,checkboxLoyaltyEnrolment,fieldDay,fieldMonth,fieldYear,radioMale,radoiFemale,fieldCellphone,checkboxTermsandconditions,fieldPassword,fieldConfPassword,checkboxNewsletter,textReqFirstname,textReqLastname,textReqEmail,textReqLangSelector,textReqDateValidFullDate,textReqGender,textReqCellphone ,buttonSubmit;
	public static String theUrl;
	
	public MyAccountRegistrationLocators()
	{
		
		theUrl = webUrl + "customer/account/create/";
		
		fieldFirstname = By.id("firstname");
		fieldLastname = By.id("lastname");    
		fieldEmail = By.id("email_address");    
		radioLangSelectorAr = By.xpath("//label[2]");
		radioLangSelectorEng = By.xpath("//li[3]/div/label");
		checkboxLoyaltyEnrolment = By.id("reward_enrolment");
		fieldDay = By.id("day");
		fieldMonth = By.id("month");
		fieldYear = By.id("year");
		radioMale = By.id("gender-1");
		radoiFemale = By.id("gender-2");
		fieldCellphone = By.id("cellphone");
		checkboxTermsandconditions = By.id("reward_agree_tc");
		fieldPassword = By.id("password");
		fieldConfPassword = By.id("confirmation");
		checkboxNewsletter = By.id("is_subscribed");
		textReqFirstname = By.id("advice-required-entry-firstname");
		textReqLastname = By.id("advice-required-entry-lastname");
		textReqEmail = By.id("advice-required-entry-email_address");
		textReqLangSelector = By.id("advice-validate-one-required-pref_lang-103");
		textReqDateValidFullDate = By.xpath("//div[5]");
		textReqGender = By.id("advice-validate-one-required-gender-1");
		textReqCellphone  = By.id("advice-required-entry-cellphone");
		buttonSubmit = By.xpath("//div[2]/button");
	
		
		if(storeview.indexOf("en") >= 0){
			setEnglishLocators();
		}else if(storeview.indexOf("ar") >= 0){
			setArabicLocators();
		}		
	}
			
	
	public void setArabicLocators(){
	}
	
	
	
	public void setEnglishLocators(){
	}
	
	
	
    	
    
}

