package wojoohLocators;

import org.openqa.selenium.By;

import WojoohMainFunctions.WojoohFunctions;


public class WojoohBackCustomersLocators extends WojoohFunctions{

	public static By tabCustomers,tabManageCustomers,fieldEmail,buttonSearch,buttonSearch2,linkEdit,tabAccInformation,fieldBpNumber,dropLoyaltyEnrolment,dropLoyaltyAgreeTC,fieldSapId;
    
    public WojoohBackCustomersLocators()
    {
    	
    	tabCustomers = By.xpath("//ul[@id='nav']/li[4]/a/span");
        tabManageCustomers = By.xpath("//ul[@id='nav']/li[4]/ul/li/a/span");
        fieldEmail = By.id("customerGrid_filter_email");
        buttonSearch = By.xpath("//div[@id='customerGrid']/table/tbody/tr/td[3]/button[2]");
        buttonSearch2 = By.xpath("//button[2]");
        linkEdit = By.xpath("//td[12]/a");
        tabAccInformation = By.cssSelector("#customer_info_tabs_account > span");
        fieldBpNumber = By.id("_accountbp_number");
        dropLoyaltyEnrolment = By.id("_accountreward_enrolment");
        dropLoyaltyAgreeTC = By.id("_accountreward_agree_tc");
        fieldSapId = By.id("_accountreward_member_id");
    	
    }

}

