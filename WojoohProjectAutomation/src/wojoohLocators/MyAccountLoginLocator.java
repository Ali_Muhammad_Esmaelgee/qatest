package wojoohLocators;
import org.openqa.selenium.By;

import WojoohMainFunctions.WojoohFunctions;

public class MyAccountLoginLocator extends WojoohFunctions{
  
	
	public static By fieldUsername,fieldPass,buttonSubmit,headerAlreadyRegistered,headerLoginPage,linkForgetPassword,buttonCreateAccount ;
	public static String theUrl;
	
	public MyAccountLoginLocator(){		
		
		theUrl = baseUrl+"customer/account/login/";		
		fieldUsername = By.id("email");
		fieldPass = By.id("pass");
		buttonSubmit = By.id("send2");
		buttonCreateAccount = By.xpath("//div[2]/a/span/span");
		headerAlreadyRegistered = By.xpath("//div[2]/div/h2");
		headerLoginPage = By.cssSelector("h1");
		linkForgetPassword = By.xpath("//div/ul/li[3]/a");
		
		if(storeview.indexOf("en") >= 0){
			setEnglishLocators();
		}else if(storeview.indexOf("ar") >= 0){
			setArabicLocators();
		}		
	}
			
	
	public void setArabicLocators(){
	}
	
	
	
	public void setEnglishLocators(){
	}
	

}