package WojoohMainFunctions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.safari.SafariDriver;

import PageObjects.WojoohBackendLoginPage;
import wojoohLocators.WojoohBackendLoginLocators;



public class WojoohFunctions {

	public static String store,storeview,website,browser,filename,baseUrl,htaccess,email,webUrl;
	public static WebDriver a;
	
	
	public static void setUpTarget(){					

		
		filename = new Exception().getStackTrace()[1].getClassName();
		filename = filename.substring(filename.indexOf(".")+1);		
			
		website = filename.substring(0,1);
		storeview = filename.substring(1,2);
		store = filename.substring(2,3); 			//UAE / KSA/ worldwide				
		browser = filename.substring(3,4);
		
		//setting up the website (staging or prod)
		if(website.indexOf("S") >= 0){
			htaccess = "stag:Dubai2015@wojooh.stag.redboxcloud.com";			
			
		}else if(website.indexOf("L") >= 0){
			htaccess = "wojooh.com";
		}
		
		
		baseUrl = "http://"+htaccess+"/";
		
		//setting up the storeview (Arabic or English)
		if(storeview.indexOf("A") >= 0){
			storeview = "ar";
		}else {
			storeview = "en";
		}
		
				
		//setting up the store (Arabic or English)		
		if(store.indexOf("U") >= 0){
			store = "ae";
		}else if(store.indexOf("K") >= 0){
			store = "sa";
		}

		
		//setting up baseurl
		if(store.indexOf("W") >= 0 && storeview.indexOf("en") >= 0){
			
			webUrl = baseUrl;
			
		}else if(store.indexOf("W") >= 0 && storeview.indexOf("ar") >= 0){
			
			webUrl = baseUrl+storeview+"/";
			
		}else{
			
			webUrl = baseUrl+store+"-"+storeview+"/";
		}
		
		
		//setting up the browser
		if(browser.indexOf("F") >= 0){
				a = new FirefoxDriver();
		} else if(browser.indexOf("C") >= 0){
				a = new ChromeDriver();
		}else if(browser.indexOf("IE") >= 0){
			    a = new InternetExplorerDriver();			    
		}else if(browser.indexOf("S") >= 0){
				a = new SafariDriver();
		}		
		
}

	
	
	public String createEmail()
	{
		email = "RBDTest_"+System.currentTimeMillis()+"@mailinator.com";
		return email;
		
	}

	
		
	public void goToPageByUrl(String accessMethod)
	{		
			a.get(accessMethod);		
	}
	
	
	
	
	}
