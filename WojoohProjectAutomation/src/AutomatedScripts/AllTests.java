package AutomatedScripts;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ expone.class, exportone.class, exptwo.class })
public class AllTests {

}
