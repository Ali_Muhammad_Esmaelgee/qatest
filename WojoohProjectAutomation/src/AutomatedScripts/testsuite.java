package AutomatedScripts;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


@RunWith(Suite.class)
@Suite.SuiteClasses({	
	exportone.class,
	expone.class,
	exptwo.class		
})

public class testsuite {}
