package AutomatedScripts;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;

import org.junit.Before;
import org.junit.After;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class exportone {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "http://stag:Dubai2015@wojooh.stag.redboxcloud.com";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testExportone() throws Exception {
    driver.get(baseUrl + "/sa-en/");
    assertEquals("Makeup", driver.findElement(By.linkText("Makeup")).getText());
    // ERROR: Caught exception [ERROR: Unsupported command [selectWindow | null | ]]
    assertEquals("Fragrance", driver.findElement(By.linkText("Fragrance")).getText());
    assertEquals("Skincare", driver.findElement(By.linkText("Skincare")).getText());
    assertEquals("Gift sets", driver.findElement(By.linkText("Gift sets")).getText());
    assertEquals("Fragrance", driver.findElement(By.xpath("(//a[contains(text(),'Fragrance')])[2]")).getText());
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
